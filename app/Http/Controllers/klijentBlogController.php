<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use Redirect;
use File;
class klijentBlogController extends Controller
{
    

	public function blog()
	{
		$blog= Blog::dohvatiSveAktivne();

        //dd($blog);
		return view('blog',compact('blog'));
	}


	public function clanak($link,$id)
	{
		$clanak=Blog::dohvatiSaId($id);
		$najnoviji=Blog::najnovijihPet();

	
		return view('clanak',compact('clanak','najnoviji'));
	}
	
}

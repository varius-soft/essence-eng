<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProizvodOpisProizvoda extends Model
{
    protected $table = 'proizvod_opis_proizvoda';

    protected $fillable = ['id_proizvod', 'id_opis_proizvoda'];

    public function napuni($id_proizvod, $id_opis_proizvoda){
        $this->id_proizvod = $id_proizvod;
        $this->id_opis_proizvoda = $id_opis_proizvoda;

        $this->save();
    }

    public static function dohvatiOpiseZaProizvod($id){
        return ProizvodOpisProizvoda::where('id_proizvod', $id)->get();
    }

    public static function obrisiOpiseZaProizvod($id){
        ProizvodOpisProizvoda::where('id_proizvod', $id)->delete();
    }
}

@extends('layout')

@section('title')
Salon Lepote -  Maderoterapija Tela, Maderoterapija Lica, BB Glow, Anticelulit Masaza, Relaks Masaza  - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Salon Lepote
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon">Salon Lepote</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/salon-lepote-maderoterapija-3.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Dobrodošli u salon lepote Essence of Beauty!
				</h2>
				
				<h5>
					Kozmetički salon Essence of Beauty nalazi se u Bulevaru Zorana Đinđića 6. Odiše toplinom i u njemu se klijenti tokom tretmana osećaju prijatno i relaksirano, a ljubaznost i profesionalnost osoblja je ključ našeg uspeha.
					<br>
					U toplom i opuštajućem ambijentu našeg salona dočekaće vas profesionalan tim iskusnih kozmetičara i fizioterapeuta, a vi se možete potpuno opustiti i beskrajno uživati u vremenu koje ste izdvojili samo za sebe. Uz stalno praćenje dinamičnog razvoja kozmetologije, tu smo da vam ponudimo široku lepezu tretmana koji  su vrlo prijatni, efikasni i utiču ne samo na vaš lepši izgled nego i na celokupan osećaj zadovoljstva .
					<br>
					Pažljivo i detaljno osluškujemo vaše želje i potrebe i kako bi mogli da vam predložimo baš one usluge koje su za vas najdelotvornije.
					<br>
					Preparati koje koristimo su za profesionalnu upotrebu i od vrhunskih proizvođača.<br>
						
						
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<h3>
					Usluge koje nudimo u salonu su:
					<br>
					<br>
				</h3>
				
				<h5>				
				    <i class="fas fa-angle-right"></i> tretmani lica – higijenski, mezoterapija, dermapen <br>
				    <i class="fas fa-angle-right"></i> svetski hit tretman: BB Glow<br>
				    <i class="fas fa-angle-right"></i> tretmani tela – vacuslim, gipsane bermude, elektrostimulacija, IR<br>
				    <i class="fas fa-angle-right"></i> masaže – relaks, sportska, terapeutska, anticelulit, limfna drenaža<br>
				    <i class="fas fa-angle-right"></i> maderoterapija – anticelulit masaža oklagijama<br>
				    <i class="fas fa-angle-right"></i> manikir (klasičan, japanski, SPA)<br>
				    <i class="fas fa-angle-right"></i> nadogradnja i izlivanje noktiju<br>
				    <i class="fas fa-angle-right"></i> trajni gel lak<br>
				    <i class="fas fa-angle-right"></i> pedikir (estetski, medicinski, SPA)<br>
				    <i class="fas fa-angle-right"></i> depilacija hladnim voskom<br>
				</h5>
				<br><br>
				<a href="/salon/cenovnik">
					Cenovnik usluga možete pogledati ovde.
				</a>
				<br>
				<br>
				<img src="{{asset('img/sectionSeparator.png')}}" alt="maderoterapija bb glow srbija">
				<br>
				<h3 style="color: #30b1bf">
					Look better, Feel better…
				</h3>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/salon-lepote-maderoterapija-2.jpg')}}" alt="">
				</div>
				<br>
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/salon-lepote-maderoterapija-1.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>




@stop




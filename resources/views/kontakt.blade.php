@extends('layout')

@section('title')
Kontakt - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'Kontakt strana');
</script>
@stop

@section('scriptsBottom')
	<script src='https://www.google.com/recaptcha/api.js' async defer></script>
@endsection

@section('sekcije')
<section id="spabreadcrumb" class="spabreadcrumb">
		<div class="bcoverlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="Content">
						<h2>
							CONTACT
						</h2>
						<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/kontakt">Contact</a>
							</li>
						</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Welcome Area End -->

<!--Start Contact Wrap-->
<div class="contact-wrap">
	<!--Start Container-->
	<div class="container">
		<!--Contact Map-->
		<!--Start Row-->
		<div class="row">
			<div class="col-md-4 d-flex align-self-center">
				<div class="address">
				<h4>Essence of Beauty</h4>
				<h3>Information</h3>
				<div class="media">
					<div class="left align-self-center mr-3">
						<i class="pe-7s-map-marker"></i>
					</div>
					<div class="media-body">
						<h4>Address</h4>
						<p>6 Zorana Djindjica Boulevard , 11000 Belgrade</p>
					</div>
				</div>
				<div class="media">
					<div class="left align-self-center mr-3">
					<i class="pe-7s-call"></i>
					</div>
					<div class="media-body">
						<h4>Phone</h4>
						<p><a href="tel:+381 62 455 200">+381 62 455 200</a></p>
					</div>
				</div>
				<div class="media">
					<div class="left align-self-center mr-3">
					<i class="pe-7s-clock"></i>
					</div>
					<div class="media-body">
						<h4>Working hours</h4>
						<p>Radnim danima od 13<sup>00</sup> do 20<sup>00</sup></p>
						<p>Subotom od 9<sup>00</sup> do 16<sup>00</sup></p>
					</div>
				</div>
				<div class="media">
					<div class="left align-self-center mr-3">
						<i class="pe-7s-mail"></i>
					</div>
					<div class="media-body">
						<h4>E-mail</h4>
						<p>info@essenceofbeauty.rs</p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-md-8">
				<!--Start Contact Form-->
				<div class="contact-form">
					<h2>Contact us</h2>
					<p>We are looking forward to your message</p>
					<form action="/kontaktiraj" id="kontakt_forma" method="POST">
						{{csrf_field()}}
						<div class="row">
							<div class="col-md-12">
								<div class="form-group form-element">
									<input type="text" class="form-control input-field" id="name" name="ime_prezime" placeholder="Name and surname*" required>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group form-element">
									<input type="email" class="form-control input-field" id="email" name="mail" placeholder="E-mail*" required>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group form-element">
									<input type="text" class="form-control input-field" id="telefon" name="telefon" placeholder="Phone*" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								
								<div class="form-group form-element">
									<textarea id="message" cols="30" rows="10" class="form-control" name="poruka" placeholder="Message*" required></textarea>
								</div>
							</div>
						</div>
						<!-- DA BI FUNKCIJA env RADILA MORA SE URADITI artisan config:clear, tek tada se config cache brise -->
						<div class="form-row">
							<div class="col">
								<div class="input-group">
									<div style="width:100%;" class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}" id="captcha">
										<div class="g-recaptcha" data-theme="light" data-sitekey="{{config('app.re_cap_site')}}" style="overflow:hidden; display:flex; justify-content:center;"></div>
										@if ($errors->has('g-recaptcha-response'))
											<br/>
											<span class="invalid-feedback" role="alert" style="display:block;">
                                                        <strong>Morate potvrditi da niste robot.</strong>
                                                    </span>
										@endif
									</div>
								</div>
							</div>


						</div>
						<div class="contact-frm-btn">
							<button type="submit" form="kontakt_forma" value="Submit" class="mr_btn_fill">Send a request </button>
						</div>
					</form>
				</div>
				<!--End Contact Form-->
			</div>
		</div>
		<!--End Row-->
	</div>
	<!--End Container-->
</div>
<!--End Contact Wrap-->
<!--End Page Content-->
<!-- google map area start -->

<div class="map-wrapper">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3365.976151895971!2d20.429736156759425!3d44.81265552139742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a655f2b30c7a5%3A0xa32ea7144201f71a!2sBulevar+Zorana+%C4%90in%C4%91i%C4%87a+6%2C+Beograd+11070!5e0!3m2!1sen!2srs!4v1554244102906!5m2!1sen!2srs" width="100%" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<!-- google map area end -->
@stop
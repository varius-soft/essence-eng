@extends('layout')

@section('title')
VAUČERI - BB EDUKACIJA - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'VAUCER BB EDUKACIJA');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						VOUCHERS – BB GLOW TREATMENT
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/vauceri">Vouchers</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/vaucer-bb-glow-edukacija">BB Glow Treatment</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-8">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/vauceri/bb-glow-edukacije-vaucer.jpg')}}" alt="">


				</div>
			</div>
	</div>
</section>
<!-- Video Section End -->


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							HOW TO USE A VOUCHER?<br>
						</h2>
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						You can take over your voucher<a onclick="fbq('track', 'VAUCER BB EDUKACIJA - PREUZET');" target="_blank" href="https://www.essenceofbeauty.rs/vaucer-bb-edukaija.pdf" >HERE</a> and bring it when you come for an education in printed form (on paper) or your phone. The voucher is valid for <strong>BB GLOW Treatments until the end of November 2019.</strong>
						You can view other vouchers <a href="/vauceri"> HERE. </a>
					</h5>
					<BR><BR>
					<h2>
						<a onclick="fbq('track', 'VAUCER BB EDUKACIJA - PREUZET');" style="color: #bb6dac;" target="_blank" href="https://www.essenceofbeauty.rs/vaucer-bb-edukaija.pdf">
							TAKE OVER YOUR VOUCHER
						</a>
					</h2>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Until when is possible to use your voucher?
	<br><br>
	</h2>
	<div class="row">
		<div class=" col-md-12">
			<div class="c-box">
				<h5 style="color: white;">Voucher is valid until the end of November 2019 and it is for BB GLOW Treatments</h5>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						
						<br>
						<h4>
							Book a date on the following phone number:
							<strong><a href="tel:062/455200">062/455200</a></strong> or via mail: <strong><a href="info@essenceofbeauty.rs">info@essenceofbeauty.rs</a></strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>

@stop




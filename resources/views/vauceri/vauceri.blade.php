@extends('layout')

@section('title')
VAUČERI - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'VAUCERI Stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						VOUCHERS
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/vauceri">Vouchers</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<!--
		<div class="row">
			<div class="col-lg-2 col-sm-0"></div>
			<div class="col-lg-4 col-sm-12">
				<div class="vImg">
					<a href="/vaucer-maderoterapija-tela">
						<img class="img-fluid" src="{{asset('img/vauceri/maderotrapija-edukacije-vaucer.jpg')}}" alt="">
					</a>

				</div>
			</div>
			<div class="col-lg-4 col-sm-12">
				<div class="vImg">
					<a href="/vaucer-bb-glow-tretman">
						<img class="img-fluid" src="{{asset('img/vauceri/bb-glow-tretman-vaucer.jpg')}}" alt="">
					</a>

				</div>
			</div>
			
	</div>
	-->
	<br>
	<h6>Trenutno nema aktuelnih vaučera.</h6>
	<div class="row">
		<div class="col-lg-2 col-sm-0"></div>
		<!--
		<div class="col-lg-4 col-sm-12">
				<div class="vImg">
					<a href="/vaucer-bb-glow-edukacija">
						<img class="img-fluid" src="{{asset('img/vauceri/bb-glow-edukacije-vaucer.jpg')}}" alt="">
					</a>

				</div>
			</div>
		
			<div class="col-lg-4 col-sm-12">
				<div class="vImg">
					<a href="/vaucer-bb-glow-tretman">
						<img class="img-fluid" src="{{asset('img/vauceri/bb-glow-tretman-vaucer.jpg')}}" alt="">
					</a>

				</div>
			</div>
			<div class="col-lg-4 col-sm-12">
				<div class="vImg">
					<a href="/vaucer-hijaluron-pen">
						<img class="img-fluid" src="{{asset('img/vauceri/hijaluron-pen-edukacija-vaucer.jpg')}}" alt="">
					</a>

				</div>
			</div>
			-->
	</div>
</section>
<!-- Video Section End -->





<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						
						<br>
						<h4>
							Book a date on the following phone number:
							<strong><a href="tel:062/455200">062/455200</a></strong> or via mail: <strong><a href="info@essenceofbeauty.rs">info@essenceofbeauty.rs</a></strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>

@stop




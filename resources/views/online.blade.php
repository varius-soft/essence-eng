@extends('layout')

@section('title')
ONLINE EDUKACIJE BB Glow - 
@stop



@section('scriptsTop')
<link rel="stylesheet" type="text/css" href="{{asset('css/cenovnik.css')}}">
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('scriptsBottom')
<script src="{{asset('js/cenovnik.js')}}"></script>
@stop



@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						ONLINE EDUKACIJE BB Glow
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/nalog">ONLINE EDUKACIJE</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li style="color: white;">
								BB Glow
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<section style="padding-top: 50px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="img/videoSection.jpg" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					 ONLINE EDUKACIJA - BB Glow
				</h2>
				<p>
					Par rečenica o edukaciji, uvodna lekcija...<br>
					Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
					aliquyam erat, sed diam Demo Label. At vero eos et accusam et justo duo dolores.
					At vero eos et accusam et justo duodolores et ea rebum.
				</p>
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>UVODNA LEKCIJA</h5>
								<p>Pogledajte uvodnu lekciu</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<!-- Video Section Start -->
<section style="padding-top: 80px; padding-bottom: 80px;">
	<div class="container">
				<button class="collapsible" style="margin-bottom:30px;">LEKCIJA 1</button>
				<div class="content" >
					<div class="row" style="padding-top: 30px; padding-bottom: 30px;">
						<div class="col-lg-6">
							<div class="vImg">
								<img class="img-fluid" src="img/videoSection.jpg" alt="">
							</div>
						</div>
						<div class="col-lg-6">
							<h2>
								 LEKCIJA 1
							</h2>
							<p>
								OPIS LEKCIJE<br>
								Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
								aliquyam erat, sed diam Demo Label. At vero eos et accusam et justo duo dolores.
								At vero eos et accusam et justo duodolores et ea rebum.
							</p>
							<div class="videoContent">
								<div class="icon">
									<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
										<i class="fas fa-play"></i>
									</a>
								</div>
								<div class="content d-flex">
									<div class="text align-self-center">
										<h5>UVODNA LEKCIJA</h5>
											<p>Pogledajte uvodnu lekciu</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<button class="collapsible" style="margin-bottom:30px;">LEKCIJA 3</button>
				<div class="content" >
					<div class="row" style="padding-top: 30px; padding-bottom: 30px;">
						<div class="col-lg-6">
							<div class="vImg">
								<img class="img-fluid" src="img/videoSection.jpg" alt="">
							</div>
						</div>
						<div class="col-lg-6">
							<h2>
								 LEKCIJA 2
							</h2>
							<p>
								OPIS LEKCIJE<br>
								Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
								aliquyam erat, sed diam Demo Label. At vero eos et accusam et justo duo dolores.
								At vero eos et accusam et justo duodolores et ea rebum.
							</p>
							<div class="videoContent">
								<div class="icon">
									<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
										<i class="fas fa-play"></i>
									</a>
								</div>
								<div class="content d-flex">
									<div class="text align-self-center">
										<h5>UVODNA LEKCIJA</h5>
											<p>Pogledajte uvodnu lekciu</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<button class="collapsible" style="margin-bottom:30px;">LEKCIJA 3</button>
				<div class="content" >
					<div class="row" style="padding-top: 30px; padding-bottom: 30px;">
						<div class="col-lg-6">
							<div class="vImg">
								<img class="img-fluid" src="img/videoSection.jpg" alt="">
							</div>
						</div>
						<div class="col-lg-6">
							<h2>
								 LEKCIJA 3
							</h2>
							<p>
								OPIS LEKCIJE<br>
								Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
								aliquyam erat, sed diam Demo Label. At vero eos et accusam et justo duo dolores.
								At vero eos et accusam et justo duodolores et ea rebum.
							</p>
							<div class="videoContent">
								<div class="icon">
									<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
										<i class="fas fa-play"></i>
									</a>
								</div>
								<div class="content d-flex">
									<div class="text align-self-center">
										<h5>UVODNA LEKCIJA</h5>
											<p>Pogledajte uvodnu lekciu</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<button class="collapsible" style="margin-bottom:30px;">LEKCIJA 4</button>
				<div class="content" >
					<div class="row" style="padding-top: 30px; padding-bottom: 30px;">
						<div class="col-lg-6">
							<div class="vImg">
								<img class="img-fluid" src="img/videoSection.jpg" alt="">
							</div>
						</div>
						<div class="col-lg-6">
							<h2>
								 LEKCIJA 4
							</h2>
							<p>
								OPIS LEKCIJE<br>
								Sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
								aliquyam erat, sed diam Demo Label. At vero eos et accusam et justo duo dolores.
								At vero eos et accusam et justo duodolores et ea rebum.
							</p>
							<div class="videoContent">
								<div class="icon">
									<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
										<i class="fas fa-play"></i>
									</a>
								</div>
								<div class="content d-flex">
									<div class="text align-self-center">
										<h5>UVODNA LEKCIJA</h5>
											<p>Pogledajte uvodnu lekciu</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<button class="collapsible" style="margin-bottom:30px;">KRAJ EDUKACIJE - SERTIFIKAT</button>
				<div class="content" >
					<div class="row" style="padding-top: 30px; padding-bottom: 30px;">
						<div class="col-lg-6">
							<div class="vImg">
								<img class="img-fluid" src="img/sertifikat.png" alt="">
							</div>
						</div>
						<div class="col-lg-6">
							<h2>
								 ČESTITAMO!<br>
								
							</h2>
							<p>
								<br>
								<strong>ZAVRŠILI STE VAŠU EDUKACIJU!</strong><br>
								Sertifikat možete preuzeti <a target="blank" href="/maderoterapija-edukaija.pdf"> OVDE.</a>
								<BR> Vaš sertifikat će UVEK biti dostupan na Vašem Essence of Beauty nalogu!<br><br>
							</p>
							<a target="blank" href="/maderoterapija-edukaija.pdf">
								<h2>PREUZMITE SERTIFIKAT</h2>
                            
                        	</a>
						</div>
					</div>
				</div>
		</div>
	</div>
</section>
				




<section id="serviceSection" class="serviceSection" >
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Za sva pitanja i podršku kontaktirajte nas na: <br><strong><a href="tel:069/455-90-99">069/455-90-99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>

@stop




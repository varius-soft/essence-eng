@extends('layout')

@section('title')
 You have successfully contacted us. - 
@stop

@section('scriptsTop')
<script>
        fbq('track', ' Izvrsili kontakt');
</script>
@stop

@section('sekcije')

<!-- SignUP Area Start -->
<section class="logRegForm">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 col-md-12">
                <div class="contact_form_wrappre2">

                        <div class="inputArea">

                            
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="sectionTheading">
                                        <h2>
                                            You have successfully contacted us.
                                            <br>
                                        </h2>
                                        
                                        <img height="300" src="{{asset('img/uspesno.png')}}" alt="">
                                        
                                        <p>
                                            <strong>
                                            We will do our best to answer you as soon as possible. <br>
                                            Essence of Beauty.
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-row">
                                <div class="col-6">
                                    <a href="/" class="lostpass">Home</a>
                                </div>
                               
                            </div>
                            
                        
                            <div class="form-row">
                                
                            </div>
                            
                
                           
                        </div>
                        

                </div>
            </div>
        </div>
    </div>


</section>
<!-- SignUp Area End -->

@stop
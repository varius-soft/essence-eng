@extends('layout')

@section('scriptsTop')
    <link href="{{asset('css/social-dugmici.css')}}" rel="stylesheet"/>
@endsection

@section('scriptsBottom')
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>
@endsection

@section('sekcije')
    <section id="spabreadcrumb" class="spabreadcrumb extrapadding">
        <div class="bcoverlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="Content">
                        <h2>
                            Registracija
                        </h2>
                        <div class="links">
                            <ul>
                                <li>
                                    <a href="/">Naslovna</a>
                                </li>
                                <li>
                                    <span>/</span>
                                </li>
                                <li>
                                    <a class="active" href="/registracija">Registracija</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Welcome Area End -->

    <!-- SignUP Area Start -->
    <section class="logRegForm">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7 col-md-10">
                    <div class="contact_form_wrappre2">

                            <div class="inputArea">
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <a href="/login" class="lostpass">Već imate nalog?</a>
                                        </div>
                                    </div>
                                    <br/>
                                <div class="form-row">

                                    <div class="col">
                                        <div class="input-group">
                                            <input id="name" type="text" class="form-control{{ $errors->has('ime_prezime') ? ' is-invalid' : '' }}" name="ime_prezime" value="{{ old('ime_prezime') }}" placeholder="Ime i prezime*" aria-describedby="username" required autofocus>

                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="username">
                                                    <i class="fas fa-user"></i>
                                                </span>
                                            </div>
                                            @if ($errors->has('ime_prezime'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('ime_prezime') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="input-group">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email*" value="{{ old('email') }}" aria-describedby="Site" required>


                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="Site">
                                                    <i class="fas fa-envelope"></i>
                                                </span>
                                            </div>
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="input-group">
                                            <input maxlength="254" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Lozinka*" aria-describedby="url" required>
                                            <div class="input-group-prepend">
														<span class="input-group-text" id="url">
															<i class="fas fa-key"></i>
														</span>
                                            </div>
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="input-group">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Potvrda lozinke*" aria-describedby="url" required>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="url">
                                                    <i class="fas fa-key"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <!-- DA BI FUNKCIJA env RADILA MORA SE URADITI artisan config:clear, tek tada se config cache brise -->
                                <div class="form-row">
                                    <div class="col">
                                        <div class="input-group">
                                            <div style="width:100%;" class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}" id="captcha">
                                                <div class="g-recaptcha" data-theme="light" data-sitekey="{{config('app.re_cap_site')}}" style="overflow:hidden; display:flex; justify-content:center;"></div>
                                                @if ($errors->has('g-recaptcha-response'))
                                                    <br/>
                                                    <span class="invalid-feedback" role="alert" style="display:block;">
                                                        <strong>Morate potvrditi da niste robot.</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>


                                </div>



                                <div class="form-row">
                                    <div class="col-md-12">
                                        <button class="loginnow" type="submit">Registracija</button>
                                    </div>
                                </div>
                        </form>
                                <div class="form-row">
                                    <div class="col text-center">
                                        <p class="loginwith" style="cursor:default;">Registracija putem društvenih mreža</p>
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col-md-6">
                                        <form action="/redirect/facebook" method="GET">
                                            <button type="submit" class="loginwithfb">
                                                Prijavite se preko Facebook-a
                                            </button>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <form action="/redirect/google" method="GET">
                                            <button type="submit" class="loginwithgoogle">
                                                Prijavite se preko Gmail-a
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- SignUp Area End -->
@stop
@extends('layout')

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/sliderPocetna.css')}}"/>
@endsection

@section('scriptsBottom')
    <script src="{{asset('js/jssor.slider-27.5.0.min.js')}}"></script>
    <script src="{{asset('js/sliderPocetna.js')}}"></script>
    <script>jssor_1_slider_init();</script>
@endsection

@section('sekcije')

    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:440px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-004-double-tail-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/double-tail-spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:500px;overflow:hidden;">
            <div>
                <!-- #endregion Jssor Slider End -->
                <section id="banner" class="banner" style=" background: url(/img/maderoterapija-bb-glow-salon-slider/ampule-za-bb-glow-prodaja-maderoterapija-bb-glow-1.jpg) no-repeat;">
                    <div class="bannerOverlay"></div>
                    <div class="container" style="position:absolute;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="content">
                                    <p class="tagline">
                                        Welcome to the company site
                                    </p>
                                    <h1>
                                        Essence Of Beauty
                                    </h1>
                                    <div class="links">
                                        <!--
                                        <a class="link1" href="/prodavnica">
                                            SHOP
                                        </a>
                                    -->
                                        <a class="link2" href="/edukacija">
                                            EDUCATION
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </div>

            <div>
                <section id="banner" class="banner" style="background: url(/img/maderoterapija-bb-glow-salon-slider/ampule-za-bb-glow-prodaja-maderoterapija-bb-glow-2.jpg) no-repeat;">
                    <div class="bannerOverlay"></div>
                    <div class="container" style="position:absolute;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="content">
                                    <p class="tagline">
                                       Welcome to the company site
                                    </p>
                                    <h1>
                                        Essence Of Beauty
                                    </h1>
                                    <div class="links">
                                    
                                        <a class="link2" href="/edukacija">
                                            EDUCATION
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div>
                <section id="banner" class="banner" style="background: url(/img/maderoterapija-bb-glow-salon-slider/ampule-za-bb-glow-prodaja-maderoterapija-bb-glow-3.jpg) no-repeat;">
                    <div class="bannerOverlay"></div>
                    <div class="container" style="position:absolute;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="content">
                                    <p class="tagline">
                                        Welcome to the company site
                                    </p>
                                    <h1>
                                        Essence Of Beauty
                                    </h1>
                                    <div class="links">
                          
                                        <a class="link2" href="/edukacija">
                                            EDUCATION
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>


<!-- 
<section id="services" class="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="sBox">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon1.png" alt="">
                    </div>
                    <h3>
                        BESPLATNA DOSTAVA
                    </h3>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sBox box2">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon2.png" alt="">
                    </div>
                    <h3>
                        EDUKACIJA
                    </h3>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sBox">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon3.png" alt="">
                    </div>
                    <h3>
                        INFO 3
                    </h3>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sBox box2">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon4.png" alt="">
                    </div>
                    <h3>
                        INFO 4
                    </h3>
                </div>
            </div>
        </div>
    </div>
</section>
 -->

<!--
<section id="giftCard" class="giftCard">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9">
                <div class="sectionTheading">
                    <h2>
                        SPECIAL OFFER
                        <br><br>
                    </h2>
                   
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/proizvod/Profesionalni-set-za-maderoterapiju-lica/10">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/maderoterapija-bb-glow-hijlaurno-pen-prodavnica/essenec-oklagije-za-lice.jpg" alt="">
                    </div>
                    <div class="cardContent">
                         <h3>
                            Set oklagija za maderoterapiju lica AKCIJA - 35%
                        </h3>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/proizvod/Dermapen-aparat/32">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/maderoterapija-bb-glow-hijlaurno-pen-prodavnica/essence-dermapen.jpg" alt="">
                    </div>
                    <div class="cardContent">
                         <h3>
                            Profesionalni Dermapen aparat AKCIJA - 10%
                        </h3>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/proizvod/Hijaluron-pen/37">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/maderoterapija-bb-glow-hijlaurno-pen-prodavnica/essence-hijaluron.jpg" alt="">
                    </div>
                    <div class="cardContent">
                         <h3>
                           Profesionalni Hijaluron Pen aparat AKCIJA - 10%
                        </h3>
                    </div>
                    </a>
                </div>
            </div>
            
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/proizvod/Drvena-oklagija-za-maderoterapiju-TIP-1/1">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/maderoterapija-bb-glow-hijlaurno-pen-prodavnica/essence-oklagija.jpg" alt="">
                    </div>
                    <div class="cardContent">
                         <h3>
                            Oklagija za maderoterapiju tela AKCIJA - 50%
                        </h3>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/proizvod/Umiruju%C4%87a-krema-posle-BB-Glow-tretmana---1-kesica/33">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/maderoterapija-bb-glow-hijlaurno-pen-prodavnica/essence-stayve.jpg" alt="">
                    </div>
                    <div class="cardContent">
                         <h3>
                            STAYVE BB Glow krema 10 + 2 na POKLON!
                        </h3>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <a href="http://essenceofbeauty.rs/proizvod/Ulje-za-maderoterapiju/34">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/maderoterapija-bb-glow-hijlaurno-pen-prodavnica/essence-ulje.jpg" alt="">
                    </div>
                    <div class="cardContent">
                         <h3>
                           Ulje za maderoterapiju - 20%
                        </h3>
                    </div>
                    </a>
                </div>
            </div>
            
        </div>
    </div>
</section>
-->
<!-- Gifts & Cards Section End -->


<!-- News Feeds Area CSS start -->
<section id="news" class="news">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9">
                <div class="sectionTheading">
                    <h2>
                        EDUCATION
                    </h2>
                    <img src="img/sectionSeparatorw.png" alt="">
                    <p>
                        Upcoming dates of our most wanted educations are: 
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
         
            
           
             <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/relaks-masaza">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/relaks-masaza-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/relaks-masaza">
                            <h3>
                               Relax Massage <br><a href="/kontakt">MAKE AN APPOINTMENT</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/anticelulit-masaza">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/anticelulit-masaza-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/anticelulit-masaza">
                            <h3>
                               Anti-celulite Massage <br><a href="/kontakt">MAKE AN APPOINTMENT</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/maderoterapija">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-tela-obuka-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/maderoterapija">
                            <h3>
                               Madero Therapy <br><a href="/kontakt">MAKE AN APPOINTMENT</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/maderoterapija-lica">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-lica-kurs-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/maderoterapija-lica">
                            <h3>
                               Face Madero Therapy  <br><a href="/kontakt">MAKE AN APPOINTMENT</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/refleksoloska-masaza">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/refleksoloska-masaza.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/refleksoloska-masaza">
                            <h3>
                               Reflexology Massage<br><a href="/kontakt">MAKE AN APPOINTMENT</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/masaza-svecom">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/masaza-svecom.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/masaza-svecom">
                            <h3>
                               Candle Massage<br><a href="/kontakt">MAKE AN APPOINTMENT</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/svedska-masaza">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/svedska-masaza.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/svedska-masaza">
                            <h3>
                               Swedish Massage<br><a href="/kontakt">MAKE AN APPOINTMENT</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>

           

            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/bb-glow">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/bb-glow-srbija-edukacija-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/bb-glow">
                            <h3>
                               BB Glow + Dermapen<br><a href="/kontakt">MAKE AN APPOINTMENT</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
        

            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/depilacija">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/depilacija-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/depilacija">
                            <h3>
                               DEPILATION<br><a href="/kontakt">MAKE AN APPOINTMENT</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            

            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/hijaluron-pen">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/hijaluron-pen2.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/hijaluron-pen">
                            <h3>
                               HYALURONIC PEN<br><a href="/kontakt">MAKE AN APPOINTMENT</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>


            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/bb-glow">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/bb-glow-srbija-edukacija-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/bb-glow">
                            <h3>
                               BB Glow + Dermapen<br><a href="/kontakt">MAKE AN APPOINTMENT</a>
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            

        </div>
    </div>
</section>
<!-- News Feeds Area CSS End -->

<!-- Video Section Start 
<section id="video" class="video">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="vImg">
                    <img class="img-fluid" src="img/videoSection.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <h2>
                    Pzicija za video
                </h2>
                <p>
                    Ovde bi bio video na kome se može prikazati obuka ili neka od usluga. Takođe možemo napraviti doodly video po potrebi gde mozemo raditi npr sta se dobija nekom od obuka sa štikliranim pozicijama...
                </p>
                <div class="videoContent">
                    <div class="icon">
                        <a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
                            <i class="fas fa-play"></i>
                        </a>
                    </div>
                    <div class="content d-flex">
                        <div class="text align-self-center">
                            <h5>obuka - Maderoterapija tela</h5>
                                <p>Pogledajte video</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
Video Section End -->


<!-- Appoinment Section Start -->
<section id="appoinmentSection" class="appoinmentSection">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="">
                    <div style="text-align: center; color: white;">
                        <h2>A place for more is needed for our education! Due to the quality of education, the work is exclusively in small groups!<br><br></h2>

                        <a href="tel:+381 62 455 200">
                            <h2>
                                +381 62 455 200
                            </h2>
                            
                        </a>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Appoinment Section End -->



<!-- Counter Area Start 
<section id="counter" class="counter">
    <div class="container">
        <div class="row">
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-diamond"></i>
                    <span class="count">1000</span><sup>+</sup>
                    <h3>Uspešnih obuka</h3>
                </div>
            </div>
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-leaf"></i>
                    <span class="count">9997</span><sup>+</sup>
                    <h3>Zadovoljnih klijenata</h3>
                </div>
            </div>
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-magic-wand"></i>
                    <span class="count">8656</span><sup>+</sup>
                    <h3>Proizvedenih oklagija</h3>
                </div>
            </div>
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-coffee"></i>
                    <span class="count">1000</span><sup>+</sup>
                    <h3>Neki info</h3>
                </div>
            </div>
        </div>
    </div>
</section>
 Counter Area End -->



<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
    <div class="container">
            <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8">
                <div class="sectionTheading">
                    <h2>
                    EDUCATION PRICES 
                    </h2>
                    <img src="{{asset('img/sectionSeparatorw.png')}}" alt="">
                    <h5>
                            Choose your own promo package with education for BB Glow Treatment:
                    </h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>START</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
                        <i class="pe-7s-diamond"></i>
                    </div>
                    <div class="doller">
                        <span><del>590 €</del></span>
                    <span>390 €</span>
                    </div>

                    <div class="list">
                        <ul>
                            <li>
                                <strong>Education</strong>
                            </li>
                            <li>
                                <strong>Studying Manual</strong>
                            </li>
                            <li>
                                <strong>CertificatE</strong>
                            </li>
                            <li>
                                <strong>Starter set</strong>
                            </li>
                            <li>
                                <strong>Support in work</strong>
                            </li>
                            <li>
                                <strong>Permanent discount of 10% for products</strong>
                            </li>
                            <li>
                                <strong><br></strong>
                            </li>
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        BOOK AN APPOINTMENT
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>SMART</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
                        <i class="pe-7s-magic-wand"></i>
                    </div>
                    <div class="doller">
                    <span><del>690 €</del></span>
                    <span>490 €</span>
                    </div>

                    <div class="list">
                        <ul>
                            <li>
                                <strong>Education</strong>
                            </li>
                            <li>
                                <strong>Studying Manual </strong>
                            </li>
                            <li>
                                <strong>Certificate</strong>
                            </li>
                            <li>
                                <strong>Premium Starter set</strong>
                            </li>
                            <li>
                                <strong>Support in work</strong>
                            </li>
                            <li>
                                <strong>Dermopen</strong>
                            </li>
                            <li>
                                <strong>Permanent discount of 10% for products</strong>
                            </li>
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        BOOK AN APPOINTMENT 
                    </a>
                </div>
            </div>
            
            
        </div>
    </div>
</section> 


<!-- Opening Time  Section Start -->
<section id="openingTime" class="openingTime">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-8">
                <div class="openingLeft">
                    <div class="left">
                        <img class="img-fluid" src="img/openingTime.jpg" alt="">
                    </div>
                    <div class="right">
                        <h2>
                            BB GLOW MADERO ANTI-CELLULITE MASSAGE
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-4">
                <div class="openingChart">
                    <div class="title">
                        <h2>
                            Working hours
                        </h2>
                    </div>
                    <div class="content">
                        <p>
                           Beauty Parlour Essence of Beauty is in 6 Zorana Djindjica Boulevard . It is filled with warmth, where clients feel pleasantly and relaxed during treatments, while kindness and professionalism of our staff present the key of our success. 
                        </p>
                        <ul class="time">
                            <li>
                                Mon-Fri: 13<sup>00</sup> – 20<sup>00</sup>
                            </li>
                            <li>
                                Saturday: 9<sup>00</sup> – 17<sup>00</sup>
                            </li>
                            <li>
                               Sunday: Closed
                            </li>
                        </ul>
                        <a class="link" href="/kontakt">
                            CONTACT
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Opening Time  Section End -->

<!-- Team Section CSS Start -->
<section id="team" class="team">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9">
                <div class="sectionTheading">
                    <h2>
                        IMPRESSIONS OF EDUCATION ATTENDERS
                    </h2>
                    <img src="img/sectionSeparatorw.png" alt="">
                    <p>
                        View impressions of some of our clients who successfully applied all knowledge acquired during our Essence of Beauty educations and earned some permanent benefits for shopping of our products.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/edukacija-kurs-maderoterapija-bb-glow-anticelulit-masaza/kurs-maderoterapija-bb-glow-1.jpg" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Svetlana Mosic
                            </h4>
                            <h6>
                                EDUCATION MADERO THERAPY
                            </h6>
                            <p>
                                

I have attended Madero Therapy education in the Educational Center Essence of Beauty.

I am more than satisfied with the education, where I have, next to a fantastic lecturer, easily acquired practical knowledge so that now I have many satisfied clients. 
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/edukacija-kurs-maderoterapija-bb-glow-anticelulit-masaza/kurs-maderoterapija-bb-glow-2.jpg" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Nikoleta Vasiljevic
                            </h4>
                            <h6>
                               EDUCATION BB GLOW, HYA PEN AND FACE MADERO THERAPY
                            </h6>
                            <p>
                               I have acquired both theoretical and practical knowledge that I started to apply immediately.
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/edukacija-kurs-maderoterapija-bb-glow-anticelulit-masaza/kurs-maderoterapija-bb-glow-3.jpg" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Sanja Stosic
                            </h4>
                            <h6>
                                EDUCATIONS ON FACE AND BODY MADERO THERAPY, BB GLOS AND DEPILATION 
                            </h6>
                            <p>
                                I am more than satisfied with the educations that I have completed in the Educational Center Essence of Beauty, lecturers and acquired knowledge. Perfect experience.  
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/edukacija-kurs-maderoterapija-bb-glow-anticelulit-masaza/kurs-maderoterapija-bb-glow-4.jpg" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Anaira Dzankovic 
                            </h4>
                            <h6>
                                BB GLOW AND HYALURONIC PEN EDUCATION
                            </h6>
                            <p>
                                I am very satisfied with the education for BB Glow and Hyaluronic Pen. All acquired knowledge is immediately applicable and of high quality.
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Team Section CSS End -->
@stop
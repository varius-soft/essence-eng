@extends('layout')

@section('title')
CAVITATION -  
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						CAVITATION
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Beauty Parlor</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/kavitacija">Cavitation</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/kavitacija-salon-beograd.png')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Cavitation
				</h2>
				<h5>
					The problem of localized layers of fat is not possible to be successfully solved with classic diets. Ultrasound lipolysis, also known as cavitation is quick, non-invasive and absolutely painless method which represents a revolutionary discovery for successful elimination of layers of fat and cellulite with the help of low-frequency ultrasound waves. With this treatment you can reduce layers of fat in the region of hips, stomach, legs and upper arms. Cavitation – ultrasound lipolysis is performed once a week. Treatments in our esthetic center imply mandatory lymph drainage (by hand or by device) as well as the recommendation for the increased inflow of liquid – mainly water, diet regime and an adequate but inevitable aerobics practice. The results are visible after the first treatment. 
					<br>
						
						
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>intro</h5>
								<h5>Play Video</h5>
						</div>
					</div>
					
				</div>
				-->
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							HOW MANY TREATMENTS ARE ENOUGH?
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						
Treatments of ultrasound lipolysis are performed once a week, with mandatory lymph drainage, which is necessary to be done immediately after the treatment. The number of treatments is individual and is specified by our therapist on consultations.

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	WHEN THE FIRST RESULTS BECOME VISIBLE?


	<br><br>
	</h2 >
	<h4 style="color: white; text-align:center;">
	The first results become visible immediately after the treatment. The volume of the treated region is smaller. 
	</h4>
</div>
</section>
<!-- Counter Area End -->


<!-- Video Section Start -->
<section id="video" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h2>
					WHAT IS DURABILITY OF THE ACHIEVED RESULTS?
				</h2>
				<h5>
					

The achieved results are long-lasting, since the destroyed cells of fat do not regenerate, so there is no possibility of ‘yo-yo’ effect. With every next treatment, body gets more and easier reshaped, also one more month after the last treatment.
			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" >
	<div class="container">
		<div class="row">
			
			<div class="col-12">
				<h2>
					WHO SHOULD NOT USE TREATMENT OF ULTRASOUND LIPOSYS?
				</h2>
				<h5>
					
Treatments of ultrasound lipolysis should not be done by pregnant women, women who are nursing babies, person suffering from diabetes, osteoporosis, kidney infections, tumors etc.
		
				</h5>
			</div>
		</div>
	</div>
</section>


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							You can book wanted treatments on the following phone number: <br><strong><a href="tel:069/455-90-99">+381 69 455 90 99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




@extends('layout')

@section('title')
PRICE LIST - 
@stop



@section('scriptsTop')
<link rel="stylesheet" type="text/css" href="{{asset('css/cenovnik.css')}}">
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('scriptsBottom')
<script src="{{asset('js/cenovnik.js')}}"></script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						PRICE LIST
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Beauty Parlor</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="" href="/salon/cenovnik">Price List</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				

				<button class="collapsible">FACE TREATMENTS</button>
				<div class="content">
					<div class="row">
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Hygienic Face Treatment</h5>  <h5 style="float: right;">RSD 3000 <br></h5> 
							<br>
							<h5 style="float: left;">Teenagers Face Treatment</h5>  <h5 style="float: right;">RSD 2700  <br></h5>
							<br>
							<h5 style="float: left;">Hydra-lift Face Treatment  </h5>  <h5 style="float: right;">RSD 3900  <br></h5>
							<br>
							<h5 style="float: left;">Anti-age Face Treatment</h5>  <h5 style="float: right;">RSD 3900 <br></h5>
							<br>
							<h5 style="float: left;">Bio-stimulating Face Treatment</h5>  <h5 style="float: right;">RSD 4500 <br></h5>
							<br>
							<h5 style="float: left;">Fruit-Acid Based Face Treatment</h5>  <h5 style="float: right;">RSD 3000 <br></h5>
							<br>
							<h5 style="float: left;">Ultrasound Face Cleansing</h5>  <h5 style="float: right;">RSD 1900 <br></h5>
							<br>
							<h5 style="float: left;">Ultrasound Cleansing with Treatment</h5>  <h5 style="float: right;">RSD 900 <br></h5>
							<br>
							<h5 style="float: left;">Face Mesotherapy </h5>  <h5 style="float: right;">RSD 3900 <br></h5>
							<br>
							<h5 style="float: left;">Face and Neck Mesotherapy</h5>  <h5 style="float: right;">RSD 4900 <br></h5> 
						</div>

				

						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Face, Neck and Cleavage Mesotherapy</h5>  <h5 style="float: right;">RSD 5900 <br></h5> 
							<br>
							<h5 style="float: left;">Dermapen Face Treatment</h5>  <h5 style="float: right;">RSD 4900 <br></h5>
							<br>
							<h5 style="float: left;">Face and Neck Dermapen Treatment</h5>  <h5 style="float: right;">RSD 5900 <br></h5>
							<br>
							<h5 style="float: left;">Face, Neck and Cleavage Dermapen Treatment</h5>  <h5 style="float: right;">RSD 6900 <br></h5>
							<br>
							<h5 style="float: left;">Face Lifting Massage</h5>  <h5 style="float: right;">RSD 800 <br></h5>
							<br>
							<h5 style="float: left;">Face Madero Therapy</h5>  <h5 style="float: right;">RSD 1300 <br></h5>
							<br>
							<h5 style="float: left;">BB Glow Treatment</h5>  <h5 style="float: right;">RSD 11900 <br></h5>
							<br>
							<h5 style="float: left;">BB Glow 2nd Treatment up to 14 days</h5>  <h5 style="float: right;">RSD 6900  <br></h5>
							<br>
							<h5 style="float: left;">BB Glow for Areas around Eyes</h5>  <h5 style="float: right;">RSD 3900  <br></h5>
							<br>
							<h5 style="float: left;">BB Glow for Areas around Eyes</h5>  <h5 style="float: right;">RSD 2500 <br></h5>
							<br>
							<h5 style="float: left;">Hyaluronic Treatment 1ml</h5>  <h5 style="float: right;">RSD 11900 <br></h5>
						</div>
					</div>
				</div>				
				<br>

				<button class="collapsible">DEPILATION FOR LADIES </button>
				<div class="content">
					<div class="row">
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Depilation (arms, legs, Brazilian wax)</h5>  <h5 style="float: right;">RSD 1950 <br></h5> 
							<br>
							<h5 style="float: left;">Depilation Half Arms</h5>  <h5 style="float: right;">RSD 300 <br></h5>
							<br>
							<h5 style="float: left;">Depilation Arms</h5>  <h5 style="float: right;">RSD 550 <br></h5>
							<br>
							<h5 style="float: left;">Depilation Half Legs</h5>  <h5 style="float: right;">RSD 550 <br></h5> 
							<br>
							<h5 style="float: left;">Depilation Full Legs</h5>  <h5 style="float: right;">RSD 950 <br></h5>
							<br>
							<h5 style="float: left;">Depilation Bikini Zone</h5>  <h5 style="float: right;">RSD 300 <br></h5>
							<br>
							<h5 style="float: left;">Depilation Bikini Zone</h5>  <h5 style="float: right;">RSD 450 <br></h5>
							<br>
							<h5 style="float: left;">Brazilian Wax Depilation</h5>  <h5 style="float: right;">RSD 900 <br></h5>  
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Depilation Back</h5>  <h5 style="float: right;">RSD 450 <br></h5> 
							<br>
							<h5 style="float: left;">Depilation Stomach</h5>  <h5 style="float: right;">RSD 450 <br></h5>
							<br>
							<h5 style="float: left;">Depilation Stomach Line</h5>  <h5 style="float: right;">RSD 300 <br></h5>
							<br>
							<h5 style="float: left;">Depilation Armpit</h5>  <h5 style="float: right;">RSD 300 <br></h5> 
							<br>
							<h5 style="float: left;">Depilation eyebrows</h5>  <h5 style="float: right;">RSD 250 <br></h5>
							<br>
							<h5 style="float: left;">Depilation Upper Lip</h5>  <h5 style="float: right;">RSD 250 <br></h5>
							<br>
							<h5 style="float: left;">Depilation Chin</h5>  <h5 style="float: right;">RSD 200 <br></h5>
							<br>
							<h5 style="float: left;">Depilation Full Face</h5>  <h5 style="float: right;">RSD 600 <br></h5> 
						</div>
					</div>
				</div>
			
				<br>
				<button class="collapsible">DEPILATION FOR GENTLEMEN</button>
				<div class="content">
					<div class="row">
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Depilation Shoulders</h5>  <h5 style="float: right;">RSD 400 <br></h5> 
							<br>
							<h5 style="float: left;">Depilation Back</h5>  <h5 style="float: right;">RSD 900 <br></h5>
							<br>
							<h5 style="float: left;">Depilation Arms</h5>  <h5 style="float: right;">RSD 800 <br></h5>   
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Depilation Chest</h5>  <h5 style="float: right;">RSD 700 <br></h5> 
							<br>
							<h5 style="float: left;">Depilation Stomach</h5>  <h5 style="float: right;">RSD 700 <br></h5>
							<br>
							<h5 style="float: left;">Depilation Full Legs</h5>  <h5 style="float: right;">RSD 1300 <br></h5>
						</div>
					</div>
				</div>
				<br>

				<button class="collapsible">PEDICURE SERVICES</button>
				<div class="content">
					<div class="row">
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Esthetic Pedicure</h5>  <h5 style="float: right;">RSD 1500 <br></h5> 
							<br>
							<h5 style="float: left;">SPA Pedicure</h5>  <h5 style="float: right;">RSD 1800 <br></h5>
							<br>
							<h5 style="float: left;">Paraffin Legs Treatment</h5>  <h5 style="float: right;">RSD 700 <br></h5>   
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Esthetic Pedicure with Long-Lasting Nail Polish</h5>  <h5 style="float: right;">RSD 1800 <br></h5> 
							<br>
							<h5 style="float: left;">SPA Pedicure with Long-Lasting Nail Polish</h5>  <h5 style="float: right;">RSD 2000 <br></h5>
							<br>
							<h5 style="float: left;">Nail Polishing</h5>  <h5 style="float: right;">RSD 300 <br></h5>
						</div>
					</div>
				</div>

				<br>

				<button class="collapsible">MANICURE SERVICES</button>
				<div class="content">
					<div class="row">
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Manicure</h5>  <h5 style="float: right;">RSD 800 <br></h5> 
							<br>
							<h5 style="float: left;">Manicure with Long-Lasting Nail Polish</h5>  <h5 style="float: right;">RSD 1200 <br></h5>
							<br>
							<h5 style="float: left;">SSPA Manicure </h5>  <h5 style="float: right;">1100 <br></h5>
							<br>
							<h5 style="float: left;">SPA Manicure with Long-Lasting Nail Polish</h5>  <h5 style="float: right;">RSD 1600 <br></h5>
							<br>
							<h5 style="float: left;">Japanese Manicure</h5>  <h5 style="float: right;">1200 <br></h5> 
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Paraffin Hand Treatment</h5>  <h5 style="float: right;">RSD 500 <br></h5> 
							<br>
							<h5 style="float: left;">Nail Polishing</h5>  <h5 style="float: right;">RSD 300 <br></h5>
							<br>
							<h5 style="float: left;">Gel Nail Strengthening</h5>  <h5 style="float: right;">RSD 1700 <br></h5>
							<br>
							<h5 style="float: left;">Removing Long-Lasting Nail Polish</h5>  <h5 style="float: right;">RSD 300 <br></h5>
							<br>
							<h5 style="float: left;">Removing Nail Extensions or Gel</h5>  <h5 style="float: right;">RSD 500 <br></h5> 
						</div>
					</div>
				</div>

				<br>

				<button class="collapsible">MASSAGES AND BODY TREATMENTS</button>
				<div class="content">
					<div class="row">
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Anti-cellulite Massage 30 min</h5>  <h5 style="float: right;">RSD 1100 <br></h5> 
							<br>
							<h5 style="float: left;">Anti-cellulite Massage 45 min</h5>  <h5 style="float: right;">RSD 1300 <br></h5>
							<br>
							<h5 style="float: left;">Madero Therapy 30min</h5>  <h5 style="float: right;">RSD 1300 <br></h5>
							<br>
							<h5 style="float: left;">Madero Therapy 45min</h5>  <h5 style="float: right;">RSD 1600 <br></h5>
							<br>
							<h5 style="float: left;">Therapeutic Massage 30min</h5>  <h5 style="float: right;">RSD 1600 <br></h5> 
							<br>
							<h5 style="float: left;">Therapeutic Massage 45min</h5>  <h5 style="float: right;">RSD 2000 <br></h5>
							<br>
							
							<h5 style="float: left;">Lymph Drainage 30min</h5>  <h5 style="float: right;">RSD 1500 <br></h5>
							<br>
							<h5 style="float: left;">Lymph Drainage 45min</h5>  <h5 style="float: right;">RSD 1900 <br></h5>  
							<br>
							<h5 style="float: left;">Relax Massage 45min</h5>  <h5 style="float: right;">RSD 1700 <br></h5> 
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Relax Massage 60min</h5>  <h5 style="float: right;">RSD 2100 <br></h5> 
							<br>
							<h5 style="float: left;">Coconut Magic Treatment 90min</h5>  <h5 style="float: right;">RSD 3100 <br></h5>
							<br>
							<h5 style="float: left;">Choco SPA Treatment 60min</h5>  <h5 style="float: right;">RSD 3300 <br></h5> 
							<br>
							<h5 style="float: left;">Moroccan SPA Treatment 90min</h5>  <h5 style="float: right;">RSD 3500 <br></h5>
							<br>
							<h5 style="float: left;">Harmony&Balance Treatment 90min</h5>  <h5 style="float: right;">RSD 3700 <br></h5>
							<br>
							<h5 style="float: left;">Muscles Electrostimulation</h5>  <h5 style="float: right;">RSD 1000 <br></h5> 
							<br>
							
							<h5 style="float: left;">Cavitation 40 min + Lymph Drainage</h5>  <h5 style="float: right;">RSD 4800 <br></h5> 
							<br>
							<h5 style="float: left;">Vacuum Losing Weight Treatment</h5>  <h5 style="float: right;">RSD 3500 <br></h5>
							<br>
							<h5 style="float: left;">Legs and Stomach Peeling</h5>  <h5 style="float: right;">RSD 1200 <br></h5>
							<br>
							<h5 style="float: left;">Full Body Peeling</h5>  <h5 style="float: right;">RSD 1500 <br></h5>
						</div>
					</div>
				</div>

				<br>

				<button class="collapsible">MASSAGE AND BODY  REATMENT PACKAGES</button>
				<div class="content">
					<div class="row">
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">10 Anti-cellulite Massage 30min</h5>  <h5 style="float: right;">RSD 9000 <br></h5> 
							<br>
							<h5 style="float: left;">10 Anti-cellulite Massage 45min</h5>  <h5 style="float: right;">RSD 10000 <br></h5>
							<br>
							<h5 style="float: left;">10 Madero Therapy 30min</h5>  <h5 style="float: right;">RSD 11000 <br></h5>   
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">10 Madero Therapy 45min</h5>  <h5 style="float: right;">RSD 14000 <br></h5> 
							<br>
							<h5 style="float: left;">10 Electrostimulations</h5>  <h5 style="float: right;">RSD 8000 <br></h5>
							
						</div>
					</div>
				</div>

				<br>
				<button class="collapsible">OTHER SERVICES</button>
				<div class="content">
					<div class="row">
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Tweezers Eyebrow Correction</h5>  <h5 style="float: right;">RSD 350 <br></h5> 
							<br>
							<h5 style="float: left;">Eyebrow Dying</h5>  <h5 style="float: right;">RSD 400 <br></h5> 
							<br>
							<h5 style="float: left;">Eyelashes Dying</h5>  <h5 style="float: right;">RSD 300 <br></h5>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 col-xl-6">
							<h5 style="float: left;">Eyebrow Henna Dying</h5>  <h5 style="float: right;">RSD 1500 <br></h5> 
							<br>
							<h5 style="float: left;">Eyebrow Full Treatment</h5>  <h5 style="float: right;">RSD 1700 <br></h5>
							<br>
							<h5 style="float: left;">Lash Lift</h5>  <h5 style="float: right;">RSD 2500 <br></h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<br>
	<br>
	<br>
	
	<div class="container" style="text-align: center;">
		<h5>
			*If you pay for a package of 3 massages in advance, we approve 15% discount<br>
			**It is necessary to use up services of paid packages within 60 days<br>
			***With duo massage, 2nd person pays only 60% of the price from the Price List<br>
		</h5>



	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							You can book wanted treatments on the following phone number: <br><strong><a href="tel:069/455-90-99">+381 69 455 90 99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




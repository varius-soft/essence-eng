@extends('layout')

@section('title')
COSMETIC TREATMENTS - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						COSMETIC TREATMENTS
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Beauty Parlor</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/kozmeticke-usluge">Cosmetic Treatments</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/depilacija-hladnim-voskom.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Cold Wax Depilation
				</h2>
				<h5>
					When it comes to depilation, we put accent on hygiene. We use high quality wax for single use only. In Essence of Beauty, beauticians take care of your skin as well, therefore using wax for sensitive skin. With depilation you thoroughly remove hairs from areas by your choice. Wax is applied in thin and even layers on the skin surface, pulled by bands with fast movements, removing hairs together with it. At the end of treatment, lotion is applied to reduce skin irritation and hydrate it. This process can treat the following areas: face (upper lip, eyebrows, and chin), arms, legs, armpit, thighs and Brazilian wax.		
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Manicure 
				</h2>
				<h5>
					To have attractive nails and well-groomed hands can increase self-confidence of every woman. In order to help you achieve it every day of the week, in Essence of Beauty we offer you treatments that meet your needs.		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/manikir.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/estetski-manikir.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Classic Manicure
				</h2>
				<h5>
					Classic Manicure is recommended to people who prefer natural look of nails. The treatment includes shaping and polishing of nail surface removing and nourishing hangnails and nail polishing. For polishing of nails you can choose between Crisnail polishes and IBD long-lasting anti-allergic polishes.		
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Spa Manicure
				</h2>
				<h5>
					Spa Manicure includes shaping and polishing nail surface, removing hangnails, peeling and massage of hands, nourishing of hangnails with oil and polishing. For nail polishing you can choose between Crisnail polishes and IBD long-lasting anti-allergic polishes.		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/spa-manikir.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/japanski-manikir.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Japanese Manicure
				</h2>
				<h5>
					Japanese Manicure represents a ritual of care, which, by applying natural products with the extract of seaweed, bamboo, red tea, cucumber, bee wax and pearl powder on nails, leaves a protective film and high gloss which lasts for 10 days.			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Paraffin Treatment
				</h2>
				<h5>
					Paraffin Treatment of hands is an ideal treatment for dry and waterless hand skin. With deep skin hydration, Paraffin Treatment warms up treated area, which brings do positive effect on bones and blood vessels. It is specially recommended to people who have problems with circulation and rheumatism.			
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/parafinsko-pakovanje-ruku.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="counter" class="counter">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2 style="color: white;">
					OTHER TREATMENTS:
					</h2>
    				<h5 style="color: white;">
				    	For all people who have a lack of time for taking care of their nails on a weekly basis, we recommend one of other treatments, thanks to which you can relax up to a month.<br> 
						-	Strengthening your natural nails<br> 
						-	Strengthening your natural nails <br> 
						-	Permanent French manicure<br> 
						<br> 
						
					</h5>
					
				</div>
			</div>
		</div>
		
	</div>
</section> 


<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/pedikir.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Pedicure
				</h2>
				<h5>
					Pedicure is a cosmetic treatment of feet and nails. It is performed for therapeutic, esthetic and medical purposes and can help in preventing nail infections. Pedicure is an excellent way to ensure the care for your feet. In Essence of Beauty, you can choose a service of pedicure according to your needs.		
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->


<!-- Video Section Start -->
<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Esthetic Pedicure
				</h2>
				<h5>
					Esthetic Pedicure represents basic care for feet, which is recommended at least once a month. In includes shaping and polishing of nail surface, removing hangnails, removing layers from heels and nail polishing. For nail polishing you can choose between Crisnail polishes and IBD long-lasting anti-allergic polishes.	
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/estetski-pedikir.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/medicinski-pedikir.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Medical Pedicure
				</h2>
				<h5>
					Medical Pedicure solves all your feet problems. We can be proud of high level of our beautician’s professionalism. The treatment includes shaping and polishing nail surface with correction of ingrown toenails, removing of thickened layers on feet, solving other feet problems and nail polishing.  For nail polishing you can choose between Crisnail polishes and IBD long-lasting anti-allergic polishes. All tools for providing this service are sterilized in our sterilizer.	
				</h5>
			</div>
			
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="serviceSection" class="video" style="background-color: #f8f8f8;">

	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					SPA Pedicure
				</h2>
				<h5>
					SPA Pedicure includes shaping and polishing of nail surface, removing hangnails, feet peeling and massage, nourishing hangnails with oil and nail polishing. For nail polishing you can choose between Crisnail polishes or IBD long-lasting anti-allergic nail polishes. 
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/spa-pedikir.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/kozmeticke-usluge/parafinsko-pakovanje-stopala.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Paraffin Feet Treatment
				</h2>
				<h5>
					Paraffin Feet Treatment is an ideal treatment for dry and waterless foot skin. With deep hydration of skin, Paraffin Treatment warms up the treat area, which brings to a positive effect on bones and blood vessels. It is specially recommended to people who have a circulation or rheumatism problems.			
				</h5>
			</div>
			
		</div>
	</div>
</section>

<!-- 
<section id="counter" class="counter">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2 style="color: white;">
					Za sve koji vole izgled savršenih stopala, u ponudi imamo i:
					</h2>
    				<h5 style="color: white;">
						-	Trajni lak na stopalima<br> 
						-	Ojačavanje gelom<br> 
						-	Trajni french<br> 

						
						
					</h5>
					
				</div>
			</div>
		</div>
		
	</div>
</section> 
-->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							You can book wanted treatments on the following phone number <br><strong><a href="tel:069/455-90-99">+381 69 455 90 99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




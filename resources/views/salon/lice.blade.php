@extends('layout')

@section('title')
FACIAL TREATMENTS - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						FACIAL TREATMENTS
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Beauty Parlor</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/tretmani-lica">Facial Treatments</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-lica.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Facial Treatments
				</h2>
				<h5>
					Face care is important for your skin health. Using right treatments adapted to your skin type is an excellent way to make sure your face attracts the attention it deserves. In Essence of Beauty all treatments are performed by professional beauticians.			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							HYGIENIC FACIAL TREATMENTS
						</h2>
					
					<h5>
						Hygienci facial treatments deeply cleanses skin, removes a layer of ‘dead’ cells on face and dirt from pores, bringing back freshness of skin. This treatment helps your clean be healthy and hydrated with even tan. It is highly recommended to women and men regardless of age.<br>
						<strong>Hygienic treatment includes: </strong><br>
						Mechanical peeling<br>
						Deep face cleansing<br>
						Applying a mask according to skin type<br>

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Pricing Plan Start -->
<section id="counter" class="counter">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2 style="color: white;">
					OTHER FACIAL TREATMENTS:
					</h2>
    				<h5 style="color: white;">
				    	Hydralift Face Treatment <br>
						Anti-age Face Treatment <br>
						Fruit Acids Face Treatment <br>
						Ultrasound Face Cleansing<br>
						Face Massage<br>
						Face Madero Therapy<br>
						
					</h5>
					
				</div>
			</div>
		</div>
		
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							You can book wanted treatments on the following phone number: <br><strong><a href="tel:069/455-90-99">+381 69 455 90 99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




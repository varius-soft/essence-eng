@extends('layout')

@section('title')
CAREER - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						CAREER
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Beauty Parlor</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/karijera">Career</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/posao-maderoterapija-bb-glow-srbija.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Become a part of Essence of Beauty Team!
				</h2>
				<h5>
					If you are a professional beautician or physiotherapist, we invite you to join you!			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							WHICH QUALITIES DO YOU NEED TO POSSESS?
						</h2>
					
					<h5>
						Positive energy, optimism, innovativeness  and inspiration you are willing to share with us<br>
					    Willingness to continually improve yourself<br>
					    Honesty and openness in communication, flexibility in demeanor<br>
					    Diploma on completed education for a beautician or physiotherapist<br>
					    Work experience of minimally one year on the position of beautician or physiotherapist<br>
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Pricing Plan Start -->
<section id="counter" class="counter">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2 style="color: white;">
					WHE ESSENCE OF BEAUTY ?
					</h2>
    				<h5 style="color: white;">
				    Permanent employment in pleasant and positive work atmosphere<br>
				    Permanent professional development on the position of beautician or physiotherapist.<br>
				    Possibility of being promoted and stimulating financial compensation.<br>
				    Payment of retirement contribution to voluntary retirement fund  “DDOR Garant”.<br>
					</h5>
					
				</div>
			</div>
		</div>
		
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Apply  for a job position to our parlor by sending your CV via e-mail to the following address: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




@extends('layout')

@section('title')
BB Glow  - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						BB GLOW
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Beauty Parlor</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/bb-glow">BB Glow</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/bb-glow-srbija-edukacija-naslovna.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					BB Glow 
				</h2>
				<h5>
					Try to imagine your face looking flawless and radiant, without everyday make-up! You think it’s impossible?
						Good news is that you don’t need to imagine it, since the most popular planetary treatment BB Glow has arrived to Serbia! Long and carefully kept secret of perfect skin tan of women from Korea is finally revealed and available to ladies in our country. Look into the revolutionary skin treatment with high quality products.<br>
						
						
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>intro</h5>
								<h5>Play Video</h5>
						</div>
					</div>
					
				</div>
				-->
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							FLAWLESS TAN WITH NO MAKE-UP?<br> YES, IT’S POSSIBLE!
						</h2>
					
					<h2>
						WHAT IS BB GLOW?
					</h2>
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						BB Glow is pleasant and safe treatment which results in even skin tan, as well as improvement of face skin tone. It is the latest trend in technology for skin care. It provides instant coverage and long-lasting beauty without excessive skin stimulation. With this treatment we get natural look of flawless and radiant face skin, lightening and toning of skin, plus coverage of flaws such as spots, freckles, redness or dark bags around eyes. On top of all mentioned, BB Glow has both hydrating and anti-age effect!
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	RESULTS OF BB GLOW TREATMENT
	<br><br>
	</h2>
	<div class="row">
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">1</span>
				<h5 style="color: white;">Face skin get instant glow, it is hydrated and fresh</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">2</span>
				<h5 style="color: white;">UDark bags around eyes, freckles, spots and acne scars are reduced</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">3</span>
				<h5 style="color: white;">The problem of hyperpigmentation and redness is solved</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">4</span>
				<h5 style="color: white;">Tiny wrinkles become invisible, BB Cream effect</h5>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Counter Area End -->


<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h2>
					What is BB Glow Treament?
				</h2>
				<h5>
					BB Glow is pleasant and safe treatment which results in even skin tan, as well as improvement of face skin tone. It is the latest trend in technology for skin care. It provides instant coverage and long-lasting beauty without excessive skin stimulation. With this treatment we get natural look of flawless and radiant face skin, lightening and toning of skin, plus coverage of flaws such as spots, freckles, redness or dark bags around eyes. On top of all mentioned, BB Glow has both hydrating and anti-age effect!		
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-12">
				<h2>
					How is BB Glow Treatment performed?
				</h2>
				<h5>
					The first step of treatment implies the preparation of skin and the process of exfoliation, where it is possible to combine different treatments, depending on the type and condition of skin. After that step number two of treatment follows and that is the intake of boosters with anti-ageing effect, as well as the effect of face skin moisturizing and lightening. Boosters are applied with the needling technique, using Dermapen device. After booster, BB Glow serum, available in several shades adapted to skin colour during treatment, is applied. BB Glow serum consists of a complex of highly concentrated active substances of herbal origin and pigments which are also taken in with the needling technique. At the very end, step number three includes final care and process of skin soothing, which we achieve with moisturizing and occlusive masks. 
				</h5>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h2>
					Is BB Glow treatment painful? 
				</h2>
				<h5>
					Mere performance of treatment is pleasant for client, regardless of the needling technique, used in the process of work. Our recommendation is the use of Nano needles which create superficial (epidermal) perforations. 
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-12">
				<h2>
					How many BB Glow treatments needs to be done to get the right effect?
				</h2>
				<h5>
					The number of treatments depends on the skin type. We recommend after the first treatment to repeat it in 14 days, and after that for a long-term effect it is necessary to repeat treatments every 4 to 6 weeks. 
				</h5>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h2>
					Can BB Glow treatment be performed on any type of skin? Are there any limits? 
				</h2>
				<h5>				
					BB Glow treatment is meant for every skin type for the purpose of getting even skin tan and improving skin tone. One needs to be cautious with reactive skin and inflammatory processes, where you need to clean and soothe skin, and only then perform BB Glow treatment.
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							You can book wanted treatments on the following phone number : <br><strong><a href="tel:069/455-90-99">+381 69 455 90 99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




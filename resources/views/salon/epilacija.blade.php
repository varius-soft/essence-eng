@extends('layout')

@section('title')
Epilacija - Salon Lepote - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Epilacija
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Salon Lepote</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/epilacija">Epilacija</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/epilacija.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					IPL Epilacija
				</h2>
				<h5>
					Trajna IPL Epilacija ili Fotoepilacija, predstavlja metodu uklanjanja neželjenih dlačica emitovanjem intezivne pulsne svetlosti (Intense Pulsed Light) na površinu kože. IPL je tehnologija emisije svetlosti visokog inteziteta u veoma kratkom vremenskom periodu (puls). Aparati IPL koriste ksenonske „blic“ lampe koje emituju celi spektar (belog) zračenja. Izdvajanjem određenih intervala talasnih dužina u zavisnosti od namene, postiže se korišćenjem odgovarajućih filtera koji propuštaju uži deo spektralne oblasti.			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
					Pri emisiji svetlosti dolazi do apsorpcije zračenja od strane pigmenta melanina. Pošto dlačice imaju veću koncentraciju melanina, one apsorbuju najveći deo zračenja (selektivna fototermoliza). Iz tog razloga koža ostaje praktično netaknuta, samim tim pojava neželjenih propratnih efekata se smanjuje na minimum.
					<br><br>

					Deo emitovane energije se pretvara u toplotnu energiju koju dlaka sprovodi do folikula. Toplota koja se razvija dovoljna je da izazove trajno oštećenje folikula.  Pošto su folikuli odgovorni za pojavu dlačica, dolazi do prestanka njihove proizvodnje.
					<br><br>

					Opseg talasnih dužina IPL aparata omogućava tretiranje dlačica različitih boja (osim sedih i veoma bledih). U zavisnosti od boje dlake i položaja (dubine) folikula u potkožnom tkivu, neke dlačice će sprovesti toplotu do folikula, a nekima će biti potrebno ponavljanje. 
					<br><br>

					Sonde IPL aparata pri upotrebi emituju svetlost talasnih dužina vidljivog (VIS) i bliskog infracrvenog (NIR) dela spektra, što znači da imaju malu energiju zračenja. Dubina prodiranja ovog zračenja su par milimetara, tako da su tkiva u unutrašnjosti potpuno zaštićena.
					<br><br>

					Za vreme IPL epilacije oseća se blago peckanje na delovima tela koji su osetljiviji na bol i toplotu ili imaju nešto deblji sloj masnih nalaga i gde je veća gustina folikula dlake. Regije sa debljim slojem masnih naslaga se brže i lakše zagrevaju, jer se toplota brzo širi ovim medijumom, a regije sa većom gustinom folikula primaju veću količinu toplotne energije po jedinici površine tkiva. Nakon tretmana moguća je pojava blagog crvenila koje prolazi do 72 sata.
					<br><br>

					Ovom metodom se tretiraju svi delovi tela osim područja oko očiju i to: nausnice, lice, vrat, pazuh, prepone, ruke, noge, grudi i leđa. IPL epilaciju mogu koristiti i žene i muškarci.
					<br><br>

					Broj tretmana je individualan i zavisi od vrste dlake, tipa kože i genetskih predispozicija. 
					<br><br>



					</h5>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Pricing Plan Start -->
<section id="counter" class="counter">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<h2 style="color: white;">
					Pre IPL epilacije potrebno je da:
					</h2>
    				<h5 style="color: white;">
				    	Izbegavate čupanje dlačica najmanje 4 nedelje pre tretmana<br>
						Dlačice treba da budu veće od 1-2 milimetra<br>
						Izbegavate izlaganje suncu ili sunčanje u solarijumu<br>
						Izbegavati korišćenje krema za samopotamljivanje<br>
						Izbegavati korišćenje antikoagulanasa<br>

						
					</h5>
					<br><br>
					<h2 style="color: white;">
					IPL epilacija se ne preporučuje u sledećim slučajevima:
					</h2>
    				<h5 style="color: white;">
				    	Osobama mlađim od 16 godina<br>
						Trudnoće i periodu dojenja<br>
						Epilepsija<br>
						Dijabetes<br>
						Multipla skleroza<br>
						Otvorene rane<br>
						Bolesti kože<br>
						Kancer<br>
						Srčana oboljenja ili pejsmejker<br>
						Stentovi<br>
					</h5>
				</div>
			</div>
		</div>
		
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Željene tretmane možete zakazati na broj: <br><strong><a href="tel:069/455-90-99">069/455-90-99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




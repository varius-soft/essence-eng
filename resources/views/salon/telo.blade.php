@extends('layout')

@section('title')
MASSAGES AND BODY TREATMENTS - 
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'SALON - stranica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						MASSAGES AND BODY TREATMENTS
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Beauty Parlor</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/salon/tretmani-tela">Massages and Body Treatments</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/masaza.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Massages and Body Treatments
				</h2>
				<h5>
					It is necessary from time to time to afford yourself a getaway from everyday life with the help of massage which satisfies all your needs. All our physiotherapists are highly qualifies for different types of massage, so we can guarantee the best from any chosen massage. Massage can help you live healthier, which includes: better circulation, reduction of blood pressure, stress and tension release, strengthening immune system, improving your sleep, flexibility and removing pain in certain regions. In Essence of Beauty all massages can be used by both men and women.		
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Anti-cellulite Massage 
				</h2>
				<h5>
					Anti-cellulite Massage is focused on the problematic areas for cellulite removal. The massage has the purpose by stimulating blood and lymph circulation to enable removal of cellulite from capsule and, with the help of organs for detoxification, release it from body. Regarding its location, 3-5 millimeters under the skin surface, within binding tissue, there is no reason to use rough pressing and damaging blood vessels, which will result in bruising. During massage we use creams, emulsions or oils. The effect of treatment is improved if you apply a repairing mask after the treatment on the treated areas.		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/anticelulit-masaza.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/maderoterapija.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Madero Therapy
				</h2>
				<h5>
					Madero Therapy is absolutely natural anti-cellulite treatment, performed by a massage with the help of specially designed rolling pins of different shapes, which provide intensive pressure on cellulite, improve circulation and releasing extra fluids and toxins from body. After just several treatments, skin becomes more radiant, tight and elastic, whereas final results are decreased volume on wanted areas, reshaped legs and lifted gluteus. Wooden elements which are used for Madero Therapy are anti-allergic, and their shape allows them easier accessibility to all areas of body that we treat.<br>
					<strong>
					With efficient removal of cellulite, Madero Therapy has more positive effects on our body:<br>
					</strong>
					-	Improves immune system<br>
					-	Creates balance between mind, body and spirit<br>
					-	Improves muscle tone<br>
					-	Regulates blood and lymph circulation<br>
					-	Decreases localized layers of fat<br>
					-	Improves the production of elastin, vitamin E and collagen, which are important for skin quality, which decreases in time<br>
	
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Therapeutic Massage
				</h2>
				<h5>
					Therapeutic Massage or partial massage is applied at the request of client and refers to the treatment of certain part of body (arms, legs, feet, back or head). With this massage different techniques, which originated from different massage styles, are used. Our physiotherapist will adjust the technique to your condition and needs. Goal of this massage is for treated area to become extra relaxed and to enable harmonic functioning of your entire body. Therapeutic Massage can be used by people who do not have a specific problem, but they want to keep a good physical shape. 		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/terapeutska-masaza.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/sportska-masaza.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Sport Massage
				</h2>
				<h5>
					Sport Massage is made to prevent and relieve the pain and discomfort from injuries and conditions related to working out or sport. It is recommended to professional sportsmen, sport amateurs and everybody whose body is exposed to greater strains. This massages provides warming-up and relaxing of tissues, which leads to improvement of circulations, evening muscle tissues and releasing lactic acids from body. <br>
					<strong>The advantages of sport massage are:</strong><br>
					-	Releasing swelling caused by sport injuries<br>
					-	Reduction of muscle tension during exercising <br>
					-	Preventing further injuries during exercising<br>
					-	Reduction of exhaustion after intensive practice<br>
		
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Relax Massage
				</h2>
				<h5>
					Relax Massage presents the right choice if you need to get rid of stress and get away from everyday life. Apart from the care for your body, it also lifts your general health condition, releases stress and relaxes all muscles. This massage starts with the gentle moves in order to find regions of tension on your body. With the help of scented oils, long movement for softening your muscles and smooth binding tissue are used to release tension. It is highly recommended to everyone.<br>
					<strong>The advantages of Relax Massage are:</strong>

					-	Improvement of circulation<br>
					-	Improvement of circulation<br>
					-	Releasing low level of pain in your muscles<br>
					-	Total relaxation and rest<br>
		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/relaks-masaza.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/kokos-masaza.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Coconut Magic Treatment
				</h2>
				<h5>
					Coconut Magic Treatment is created especially for you who next to full relaxation want to provide maximal high quality care for your skin. It is about special type of anti-stress massage using organic cold pressed coconut oil, which will deeply hydrate and nourish your skin, provide it with enough quantity of vitamin E and protect it from microbes and infections. Whole treatments lasts 60 minutes.
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Choco SPA Treatment
				</h2>
				<h5>
					Choco SPA Treatment is the easiest way to achieving relaxation. A combination of Relax Massage with chocolate oil, body peeling, applying a chocolate cream for body care is just an introduction of this spa treatment. We also thought about your face, which we prepare by cleansing, using milk and tonic. After that, we apply chocolate mask, followed by cream and anti-wrinkle eye cream, according to your skin type. To make the pleasure complete, there is also a hand paraffin treatment. Whole treatment lasts 60 minutes, out of which 45 minutes represent a Relax Massage.		
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/cokolada-masaza.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/marocan.jpeg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Moroccan SPA Treatment
				</h2>
				<h5>
					Moroccan SPA Treatment consists of an Argan Oil Relax Massage, also called in Morocco a liquid gold, due to its fantastic effects. After massage a body peeling follows, which will clean, hydrate and give your skin a fresher look, making it softer and smooth looking. The treatment ends with argan body cream application and paraffin hand treatment. Whole treatment lasts 60 minutes, out of which 45 minutes represent a Relax Massage.	
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					Duo Relax Massage
				</h2>
				<h5>
					Duo Relax Massage or massage in pair is becoming more and more wanted service that we provide for our clients. With this massage two people get a massage in the same room, on two massage tables, at the same time, by two therapists. This massage gives you a unique opportunity to spend wonderful moments with your close person, chatting and relaxing together in silence. The choice is yours. 
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/duo-masaza.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/elektrostimulacija.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Electrostimulation of Muscles
				</h2>
				<h5>
					Electrostimulation of Muscles is a treatment which, using devices and low frequency power, lifts body muscles tone. Stimulating muscles of certain regions, circulation is improved and there is better tissue drainage and rapid fat burning. When contract, muscles use up energy from their own glycogen and surrounding layers of fat. In that way, reduction of layers of fat is stimulated and can be minimized in2-3 cm. The following regions can be treated: gluteus, legs, stomach and arms. The treatment is recommended to both ladies and gentlemen.			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="video" style="background-color: #f8f8f8;">
	<div class="container">
		<div class="row">
			
			<div class="col-lg-6">
				<h2>
					IR Detox Body Treatment
				</h2>
				<h5>
					IR Detox Body Treatment is excellent for burning out calories and releasing body from unwanted toxins. The treatment helps you relax, soothe pain, burn fat, increase circulation, improve skin texture, enhance energy, strengthen metabolism and decrease the level of stress.<br>
					Infra-red blanket is made out of two losing weight zones, where front and back part of your body evenly lose weight. When you lie down, the pressure on your spine is half smaller, muscles get weaker and the whole body relaxes. Blood circulates better and the strain on your heart lessens. Capillaries widen, which enables oxygen to flow more efficiently through whole body.<br>
					Infra-red blanket is the solution for healthy losing weight, without exhausting working out. During only one treatment of 30 minutes, you can burn out minimally 600 calories. After first treatment, you can irrecoverably lose 1-4cm (250-300g).
					<strong>
					Contra-indications (necessary consulting of doctor prior to treatment in the following cases): <br>
					</strong>
					-	Epilepsy<br>
					-	Heart diseases or pacemaker<br>
					-	Hypertension<br>
					-	Pregnancy or lactation<br>
			
				</h5>
			</div>
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/infra.jpg')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					
					<img class="img-fluid" src="{{asset('img/salon/tretmani-tela/piling-tela.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Body Peeling
				</h2>
				<h5>
					Body peeling removes ‘dead’ cells from your skin, leaving it soft, smooth and fresh. During treatment, peeling is slowly being rubbed in and massaged over skin to necessary areas, and then rinsed. It represents good basis for other body treatments. It also improves blood and lymph circulation on the surface of skin, helping it in the fight against cellulite and influences improvement of skin tan.			
				</h5>
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->

<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							You can book wanted treatments on the following phone number <br><strong><a href="tel:069/455-90-99">+381 69 455 90 99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




<div id="tabela-vauceri">
<h5>Vaučeri</h5>
<table class="table" >

    <thead>
    <tr>
        <th></th>
        <th>Kod vaučera</th>
        <th>Iznos</th>
    </tr>
    </thead>
    <tbody>
        @foreach($stavkeVauceri as $stavkaVaucer)
            <tr id="stavka-vaucer-{{$stavkaVaucer->rowId}}">
                @include('include.popupDialog', ['poruka' => 'Da li ste sigurni da želite da uklonite vaučer?', 'linkUspesno' => 'javascript:ukloniVaucer(\''. $stavkaVaucer->rowId . '\',\'' . $stavkaVaucer->id .'\')', 'dialogId' => 'vaucer-' . $stavkaVaucer->rowId])
                <td style="width:43%">
                    <div class="product-details"><!-- product details -->
                        <div class="close-btn cart-remove-item" onclick="otvoriDialogSaId('vaucer-{!! $stavkaVaucer->rowId!!}')">
                            <i class="fas fa-times" ></i>
                        </div>
                        <div class="content">
                            <h4 class="title">Vaučer</h4>
                        </div>
                    </div><!-- //. product detials -->
                </td>
                <td style="width:42%">
                    <div style="margin-left:15px;" class="price">{{$stavkaVaucer->name}}</div>
                </td>
                <td style="width:15%">
                    <div style="margin-left:15px;" class="price">-{{number_format($stavkaVaucer->price, 2, ',', '.')}} rsd</div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>

@extends('layout')

@section('title')
ANTI-CELLULITE MASSAGE -
@stop


@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Anticelulit');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						ANTI-CELLULITE MASSAGE
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Education</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/anticelulit-masaza">Anti-Cellulite Massage</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">

			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/anticelulit-masaza.jpg')}}" alt="">
				</div>
			</div>
			
			<div class="col-lg-6">
				<h2>
					Anti-Cellulite Massage
				</h2>
				
				<h5>
					Among women there is a popular opinion that hand Anti-Cellulite Massage is the most efficient way of removing cellulite. Goal of our education is to teach you to accurately perform Anti-Cellulite Massage so that client’s body could be absolutely free of the so-called orange peel.<br>
						
						
				</h5>
			<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<p>Play</p>
						</div>
					</div>
				</div>
			-->
			</div>
			
		</div>
	</div>
</section>
<!-- Video Section End -->



<!--
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							
							Termin sledeće grupne edukacije u Beogradu:<br>
							20. april 2019. godine
							
							Anticelulit masaža
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Kod žena je utemeljeno mišljenje da je ručna anticelulit masaža najefikasniji način uklanjanja celulita. Cilj naše edukacije je da vas naučimo kako da pravilno sprovodite anticelulit masažu kako bi se telo klijenta potpuno oslobodilo tzv narandžine kore.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>
-->
<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Theme units we cover: 
	<br><br>
	</h2>
	<div class="row">
		<h5 style="color: white;">
			<strong>2 CLASSES  </strong>– theoretical lectures -  History and Development of Massage, Useful Massage Effects, Ambience and Equipping Massage Premises, Skin, Cellulite- Definition, Causes and Most Frequent Regions of its Appearance, Means of Massage, Massage Effects, Dosing in Massage, Indications and Counter-Indications, Preparation for Massage.
			<br>

			<strong>8 CLASSES </strong>– practical lessons – Basic Massage Moves in Treating Cellulite, Basic Massage In-Between Moves in Treating Cellulite. <br>
			After training the attendants take theoretical and practical exam and acquire the certificate on professional capability for work as an Anti-Cellulite Massage Therapist.
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
						<h2>
							DURATION OF EDUCATION

						</h2>
					
					<h5>
						Total lesson fund of this education is 10. Lessons are organized with one day, starting from 10 a.m. Provided refreshment during breaks.
						<br><br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							NECESSARY DOCUMENTS FOR APPLYING:
						</h2>
					
					<h5>
						When applying you need to submit the following: a photocopy of the previously acquired diploma of regular education and a photocopy of you ID.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					EDUCATION PRICE 
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-diamond"></i>
					</div>
					<div class="doller">
					<span>RSD 25.000</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Education</strong>
							</li>
							<li>
								<strong>Studying Manual</strong>
							</li>
							<li>
								<strong>Payment in 2 installments</strong>
							</li>
							
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						BOOK A DATE
					</a>
				</div>
			</div>
			
		</div>
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




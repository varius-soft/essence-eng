@extends('layout')

@section('title')
FACE MADERO THERAPY
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Maderoterapija Lica');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						FACE MADERO THERAPY
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Education</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/maderoterapija-lica">Face Madero Therapy</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-lica-kurs-naslovna.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Face Madero Therapy
					
				</h2>
				<h5>
					Professional education for Face Madero Therapy for amazing RSD 12,000!
					<br>You can book dates by calling on the following number: +381 62 455 200
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<h5>Play</h5>
						</div>
					</div>
				</div>
				-->
			</div>
			<div class="col-lg-12">
				<h5>
					<br>
					
				</h5>
				
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							GET EDUCATED FOR FACE MADERO THERAPIST
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Apart Body Madero Therapy which has become absolutely the most popular technique of cellulite removing, Face Madero Therapy is also becoming more and more wanted treatment in beauty parlors. Those who have tried Facial Madero Therapy made sure that it leads to improvement of face skin tone and provides the effect of rejuvenation since it stimulates the production of elastin and collagen.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Theme units that we cover:
	<br><br>
	</h2>
	<div class="row">
		<h5 style="color: white;">
			<strong>1 CLASS </strong>- theoretical lecture – Skin, Origin and Effects of Madero Therapy, Equipping Premises for Madero Therapy, Means of Madero Therapy, Madero Therapy Effects, Madero Therapy Dosing, Indications and Counter-Indications of Madero Therapy Application.
			<br>

			<strong>3 CLASSES  </strong>- practical lessons – Techniques of Facial Madero Therapy
			<br>
			After training the attendants take theoretical and practical exam and acquire the certificate on professional capability for work as a Face Madero Therapist.
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
						<h2>
							DURATION OF EDUCATION:
						</h2>
					
					<h5>
						Total number of classes of this education is 4. Classes are organized within one day, starting from 10 a.m.  Refreshment during break has been provided.
						<br><br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							NECESSARY DOCUMENTS FOR APPLYING:
						</h2>
					
					<h5>
						When applying you need to submit the following: a photocopy of the previously acquired diploma of regular education and a photocopy of you ID.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					EDUCATION PRICE 
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>START</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-diamond"></i>
					</div>
					<div class="doller">
					<span>RSD 12.000</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Education</strong>
							</li>
							<li>
								<strong>Studying Manual</strong>
							</li>
							<li>
								<strong>Certificate</strong>
							</li>
							<li>
								<strong><BR><BR></strong>
							</li>
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						BOOK A DATE	
					</a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>SMART</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-magic-wand"></i>
					</div>
					<div class="doller">
					<span><DEL>RSD 19.000</DEL></span>
					<span>RSD 12.000</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Education</strong>
							</li>
							<li>
								<strong>Studying Manual</strong>
							</li>
							<li>
								<strong>Certificate</strong>
							</li>
							<li>
								<strong>5 wooden madero elements for work</strong>
							</li>
							
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						BOOK A DATE	
					</a>
				</div>
			</div>
			
		</div>
		<div class="row justify-content-center">	
			<h5>
				<br>
				*It is necessary to bring one person who will be your model for practice on your training.<br>
				Price for individual education is 30% higher.
			</h5>
		</div>

		


	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong> <a href="tel:062/455200">062/455200</a> </strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




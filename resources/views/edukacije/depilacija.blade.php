@extends('layout')

@section('title')
DEPILATION
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Hijaluron pen');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						DEPILATION
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Education</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/depilacija">Depilation</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/depilacija.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Number of attendants in a group is small due to quality of work!
				</h2>
				<h5>
					Cold wax depilation is one of the most wanted services in beauty parlors. That is the procedure which thoroughly removes hairs in the regions of face or body, as client chooses. Wax is applied in a thin and even layer on the skin surface, upon which a strip is pressed and then quickly removed using fast hand movement, removing hairs together with strips.


				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<h5>Play</h5>
						</div>
					</div>
				</div>
				-->
			</div>
			<div class="col-lg-12">
				<h5>
					<br>
					
				</h5>
				
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					
						<h2>
							MASTER THE TECHNIQUE OF COLD AND WARM DEPILATION!
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						The goal of our education is to teach you how to properly and thoroughly perform treatments of face and body depilation. Throughout education you will get to know the procedures of warm and cold depilation together with build and types both of skin as well as hairs, and thorough disinfection and protection from irritation and infection. <br>

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Theme units we cover:
	<br><br>
	</h2>

	<div class="row">
		<h5 style="color: white;">
			<strong>2 CLASSES  </strong>– theoretical lectures: Introduction, Histology  of Skin and Hair Build, Hair Types, Disinfection and Sterilization, Basic Measures of Protection from Infection, Working Tools, Reception of Clients
			<br>

			<strong>8 CLASSES </strong>- practical lessons: Procedure of working with warm and cold wax – depilation of legs, arms, intimate, bikini zone (Brazilian depilation), eyebrows, upper lips, face, chest, stomach, back.
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
						<h2>
							DURATION OF EDUCATION

						</h2>
					
					<h5>
						Total lesson fund of this education is 10. Lessons are organized with one day, starting from 10 a.m. Provided refreshment during breaks. 
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							NECESSARY DOCUMENTS FOR APPLYING:
						</h2>
					
					<h5>
								
						When applying you need to submit the following: a photocopy of the previously acquired diploma of regular education and a photocopy of you ID.

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					EDUCATION PRICE
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>SMART</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-magic-wand"></i>
					</div>
					<div class="doller">
					<span>RSD 17.000</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Education</strong>
							</li>
							<li>
								<strong>Studying Manual</strong>
							</li>
							<li>
								<strong>Certificate</strong>
							</li>
							</li>	

						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						BOOK A DATE	
					</a>
				</div>
			</div>
			
		</div>
		<!--
		<div class="row justify-content-center">	
			<h5>
				<br>
				*Na individualnu obuku je potrebno dovesti osobu koja će biti model za vežbu.<br>
				Cena za individualnu edukaciju je 30% veća.
			</h5>
		</div>
		-->
		


	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




@extends('layout')

@section('title')
BB Glow -
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - BB Glow');
</script>
@stop

@section('sekcije')

<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						BB Glow
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Education </a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/bb-glow">BB Glow</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
 
<!--
<section id="" class="">
	<img width="100%" src="{{asset('img/bb-glow-srbija-promo.jpg')}}" alt="">

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					
						<h2><br>
							 POPUST 100€ ZA BB GLOW EDUKACIJE<br>
						</h2>
					
					<h3>
							ZA UPLATE DO 10.8.
					</h3>
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h3>
						Jedinstveni koncept edukacije u Srbiji.<br>
						Svaki polaznik samostalno radi tretman uz nadzor.<br>
						<strong>Svaki polaznik dobija tretman na poklon!</strong>
					</h3>
					<br>
					<a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
				</div>
			</div>
		</div>
	</div>
</section>
-->

<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/bb-glow-srbija-edukacija-naslovna.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					BB Glow 
				</h2>
				<h5>
					Try to picture in your head your face looking flawless and radiant, without everyday make-up! You think it’s impossible? Good news is that you don’t need to picture it in your head because planetary most popular treatment BB Glow has arrived to Serbia! Long and carefully kept secret of perfect tan of women from Korea is finally revealed and it is available to ladies in our country. Explore new revolutionary treatment of skin with high quality products.<br>
						
						
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>intro</h5>
								<h5>Play Video</h5>
						</div>
					</div>
					
				</div>
				-->
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							FLAWLESS TAN WITH NO MAKE-UP?<br> YES, IT’S POSSIBLE!
						</h2>
					
					<h2>
						WHAT IS BB GLOW?
					</h2>
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						BB Glow is pleasant and safe treatment thanks to what we achieve even skin tan, as well as improvement of face skin tone. It is the latest trend in skin care technology. It provides instant coverage and long-term beauty without excessive skin stimulation. We this treatment we achieve natural look of flawless and radiant face skin, slightly lightening up and toning skin, plus covering up all flaws such as spots, freckles, redness or dark  bags under eyes. On top of all mentioned, BB Glow also has hydrating anti-age effect!
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	RESULTS OF BB GLOW TREATMENT
	<br><br>
	</h2>
	<div class="row">
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">1</span>
				<h5 style="color: white;">Face skin gets instant glow, it’s hydrated and firm</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">2</span>
				<h5 style="color: white;">Bags under eyes, freckles, spots and acne scars are alleviated</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">3</span>
				<h5 style="color: white;">The problem of hyperpigmentation and redness is solved</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">4</span>
				<h5 style="color: white;">Tiny wrinkles become invisible, BB Cream effect</h5>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Counter Area End -->

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					EDUCATION PRICE
					</h2>
					<img src="{{asset('img/sectionSeparatorw.png')}}" alt="">
					<h5>
							Pick your own promo package with education for BB Glow treatment:
					</h5>
				</div>
			</div>
		</div>
		<div class="row">
			<!--
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>basic</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
                        <i class="pe-7s-leaf"></i>
                    </div>
                    <div class="doller">
                    <span>290 €</span>
                    </div>

                    <div class="list">
                        <ul>
                            <li>
                                <strong>Workshop</strong>
                            </li>
                            <li>
                                <strong>Priručnik za učenje </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            <li>
                                <strong>Specijalni popust za nabavku materijala za rad na dan workshopa<br><br></strong>
                            </li>
                            <li>
                                <strong><br></strong>
                            </li>
                            
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
        	-->
        	<div class="col-lg-2"></div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>START</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
                        <i class="pe-7s-diamond"></i>
                    </div>
                    <div class="doller">
                    	<span><del>€ 590 </del></span>
                    <span>€ 590 </span>
                    </div>

                    <div class="list">
                        <ul>
                            <li>
                                <strong>Education</strong>
                            </li>
                            <li>
                                <strong>Studying Manual</strong>
                            </li>
                            <li>
                                <strong>Certificate</strong>
                            </li>
                            <li>
                                <strong>Starter Set</strong>
                            </li>
                            <li>
                                <strong>Starter Set</strong>
                            </li>
                            <li>
                                <strong>Permanent 10% discount on products</strong>
                            </li>
                            <li>
                                <strong><br></strong>
                            </li>
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        BOOK A DATE 
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>SMART</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
                        <i class="pe-7s-magic-wand"></i>
                    </div>
                    <div class="doller">
                    	<span><del>€ 690 </del></span>
                    <span>€ 490 </span>
                    </div>

                    <div class="list">
                        <ul>
                            <li>
                                <strong>€ 690 </strong>
                            </li>
                            <li>
                                <strong>Studying Manual  </strong>
                            </li>
                            <li>
                                <strong>Certificate</strong>
                            </li>
                            <li>
                                <strong>Premium Starter Set</strong>
                            </li>
                            <li>
                                <strong>Support in work</strong>
                            </li>
                            <li>
                                <strong>Dermapen</strong>
                            </li>
                            <li>
                                <strong>Permanent 10% discount on products</strong>
                            </li>
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        BOOK A DATE 
                    </a>
                </div>
            </div>
            
        </div>

        <div class="row justify-content-center">	
			<h5>
				<br>
				*
				Cena za individualnu edukaciju je za 100 eura veća u odnosu na redovnu cenu.
			</h5>
		</div>
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Apply now for our education which is absolutely unique in Serbia in quality of work with attendants and the level of acquired knowledge on the following number: <strong><a href="tel:062/455200">+38162 455 200 </a></strong> or by mail:  <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




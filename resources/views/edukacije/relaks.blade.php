@extends('layout')

@section('title')
RELAX MASSAGE -
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Relaks masaza');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						RELAX MASSAGE
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Education</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/relaks-masaza">Relax Massage</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/relaks-masaza.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Relax Massage
				</h2>
				
				<h5>
					If you have showed so far preference towards massage and you really want to deal with it, then this is your unique opportunity to master this Relax Massage. Due to everyday omnipresent stress, greater number of people is becoming more exhausted, tense and in great need for relaxation. We will teach you how to transfer your positive energy onto your clients and help them in a way to improve their psychic and physical condition.<br>
						
						
				</h5>
			
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<h5>Play</h5>
						</div>
					</div>
				</div>
				-->
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							GET EDUCATED FOR A RELAX MASSAGE THERAPIST
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Goal of our education is to teach you how to correctly perform massage so that client’s body becomes absolutely relaxed. During the education you will get to know anatomy and physiology of human body, with techniques of massage and complex effects of treatment.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Theme units that we cover:
	<br><br>
	</h2>
	<div class="row">
		<h5 style="color: white;">
			<strong>3 CLASSES  </strong>– theoretical lectures – Historical Development of Massage, Basis and Classification of Massage, Equipping premises for Massage, Means of Massage, Massage Effects, Dosing in Massage, Indications and Counter-Indications, Preparation for Massage, Basic Massage Moves, Basic Massage In-between Moves.
			<br>

			<strong>9 CLASSES </strong>– practical lessons – Head, Neck and Chest Massage, Lower Extremities Massage, Upper Extremities Massage,  Chest and Stomach Massage, Back Massage.
			<br>
			After training the attendants take theoretical and practical exam and acquire the certificate on professional capability for work as a Masseur for Relax Massage.
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
						<h2>
							DURATION OF EDUCATION:
						</h2>
					
					<h5>
						Total number of lessons is 12, organized in 3 days, with 4 classes each day.
						<br><br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							NECESSARY DOCUMENTS FOR APPLYING:
						</h2>
					
					<h5>
						When applying you need to submit the following: a photocopy of the previously acquired diploma of regular education and a photocopy of you ID.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					EDUCATION PRICE
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-diamond"></i>
					</div>
					<div class="doller">
					<span>RSD 27.000</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Education</strong>
							</li>
							<li>
								<strong>Studying Manual</strong>
							</li>
							<li>
								<strong>Payment in 2 installments</strong>
							</li>
							
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						BOOK A DATE	
					</a>
				</div>
			</div>
			
		</div>
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




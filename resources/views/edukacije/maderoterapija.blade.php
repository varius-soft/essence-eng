@extends('layout')

@section('title')
MADERO THERAPY
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Maderoterapija tela');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						MADERO THERAPY
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Education</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/maderoterapija">Madero Therapy</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-tela-obuka.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Madero Therapy
				</h2>
				<h5>
					Madero Therapy is a completely natural anti-cellulite treatment where massage is performed with specially designed rolling pins of different shapes, which provides intensive pressure on cellulite, improves circulation and releases extra liquids and toxins from body. 
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<h5>Play</h5>
						</div>
					</div>
				</div>
				-->
			</div>
			<div class="col-lg-12">
				<h5>
					<br>
					
				</h5>
				
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							GET EDUCATED FOR BODY MADERO THERAPIST
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						You have heard that anti-cellulite massage with the rolling pin is the most efficient way for cellulite elimination, and it is also the most wanted service in beauty parlors? You really want to deal with this very profitable profession but you have nobody to learn from? Please contact us as soon as possible since todays starts application for training of madero therapists in our educational centers.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Theme units that we cover:
	<br><br>
	</h2>
	<div class="row">
		<h5 style="color: white;">
			<strong>2 CLASSES </strong>- theoretical lectures – Skin, Causes of Cellulite and Regions of its Appearance, Types of Cellulite, Equipment and Premises for Madero Therapy, Means of Madero Therapy, Madero Therapy Effects, Madero Therapy Dosing, Indications and Counter-Indications of Madero Therapy Application, Procedure after Madero Therapy.
			<br>

			<strong>8 CLASSES  </strong>- practical lessons – Techniques of Madero Therapy in treating cellulite
			<br>
			After training attendants take an exam in theoretical and practical part and acquire a certificate on professional capability for working as a Madero therapist.
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
						<h2>
							TIME OF EDUCATION
						</h2>
					
					<h5>
						Total fund of lessons for the education is 10. Lessons are realized within a day, starting from 10 a.m. Provided refreshment during  breaks.
						<br><br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							NECESSARY DOCUMENTS FOR APPLICATION:
						</h2>
					
					<h5>
						When applying it is necessary to submit the following: a photocopy of previously acquired diploma of regular education and a photocopy of your ID.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					EDUCATION PRICE
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>START</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-diamond"></i>
					</div>
					<div class="doller">
						<span><del>RSD  25.000</del></span>
					<span>RSD  25.000</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Education</strong>
							</li>
							<li>
								<strong>Studying Manual </strong>
							</li>
							<li>
								<strong>Certificate</strong>
							</li>
							<li>
								<strong><BR></strong>
							</li>
							<li>
								<strong><BR></strong>
							</li>
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						BOOK A DATE
					</a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>SMART</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-magic-wand"></i>
					</div>
					<div class="doller">
						<span><del>RSD 39.000</del></span>
					<span>RSD 29.000</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Education</strong>
							</li>
							<li>
								<strong>Studying Manual</strong>
							</li>
							<li>
								<strong>Certificate</strong>
							</li>
							<li>
								<strong>Professional set of 6 rolling pins</strong>
							</li>
							<li>
								<strong>500ml of oil for Madero Therapy</strong>
							</li>							


						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						BOOK A DATE	
					</a>
				</div>
			</div>
			
		</div>
		<div class="row justify-content-center">	
			<h5>
				<br>
				*It is necessary to bring a person as a model for practice.<br>
				Price for individual education is 30 % higher.
			</h5>
		</div>

		


	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




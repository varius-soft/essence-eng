@extends('layout')

@section('title')
Plazma pen Edukacija Srbija Beograd - Plazma pen
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Plazma pen');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Plazma pen
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Edukacija</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/plazma-pen">Plazma pen</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/plazma-pen.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Edukujte se za stručno izvođenje trenutno najinovativnijeg tretmana za podmlađivanje! <br>Postanite i Vi Plasma pen Expert!
				</h2>
				<h5>
					



				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<h5>Play</h5>
						</div>
					</div>
				</div>
				-->
			</div>
			<div class="col-lg-12">
				<h5>
					<br>
					
				</h5>
				
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					
						<h2>
							Tretman plazma pen aparatom je trenutno najinovativniji tretman za zatezanje i podmlađivanje, koji je efikasan u korekciji bora, ožiljaka i pigmentnih fleka.
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						
					<br>
					Cilj tretmana plazma pen aparatom je da se energijom plazme stimulišu regenerativni procesi u koži, podstakne intenzivna sinteza kolagena, elastina, i povećava apsorpcija aktivnih supstanci uz pomoć odgovarajućeg seruma. Rezultat ovog tretmana je momentalno zategnuta koža, vidljivo pliće bore i rešena dugogodišnja problematika.

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Tematske jedinice koje obrađujemo:
	<br><br>
	</h2>

	<div class="row">
		<h5 style="color: white;">
			<strong>3 ČASA </strong>- teorijska predavanja: Koža, Anatomija lica, Područje primene plazma pena, Indikacije, Kontraindikacije
			<br>

			<strong>7 ČASOVA </strong>- praktična nastava - Ovaj deo edukacije se radi na modelima.
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->



<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					Cena edukacije 
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>START</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-diamond"></i>
					</div>
					<div class="doller">
					<span>390 €</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Priručnik za učenje</strong>
							</li>
							<li>
								<strong>Sertifikat</strong>
							</li>
							<li>
								<strong><BR></strong>
							</li>

							<li>
								<strong><BR><BR></strong>
							</li>
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>SMART</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-magic-wand"></i>
					</div>
					<div class="doller">
						<span>450 €</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Priručnik za učenje</strong>
							</li>
							<li>
								<strong>Sertifikat</strong>
							</li>
							<li>
								<strong>Plazma pen aparat</strong>
							</li>
							<li>
								<strong>5 iglica za plazma pen aparat</strong>
							</li>							

						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			
		</div>
		
		<div class="row justify-content-center">	
			<h5>
				<br>
				*
				Cena za individualnu edukaciju je za 100 eura veća u odnosu na redovnu cenu.
			</h5>
		</div>
		
		


	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




@extends('layout')

@section('title')
HYALURONIC PEN
@stop

@section('scriptsTop')
<script>
    	fbq('track', 'EDUKACIJA - Hijaluron pen');
</script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						HYALURONIC PEN
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Home</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Education</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/hijaluron-pen">Hyaluronic Pen</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/hijaluron-pen.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					The number of attendants in a group is small due to work quality!
				</h2>
				<h5>
					Practical part of education is done on models. <br>
					Education consists of theoretical and practical part.<br>
					Theoretical part: Skin, Face Anatomy, Region of Hyaluronic Pen Application, Indications and Counter-Indications.<br>


				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<h5>Play</h5>
						</div>
					</div>
				</div>
				-->
			</div>
			<div class="col-lg-12">
				<h5>
					<br>
					
				</h5>
				
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					
						<h2>
							MASTER NEW REVOLUTIONARY TECHNIQUE OF APPLYING HYALURON WITHOUT NEEDLE UNDER PRESSURE! 
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Using Hyaluronic Pen you can inject hyaluronic acid into skin without any pins, evenly distributing hyaluronic acid with no lumps.<br>
						This method is gentle and almost painless and it mutually reconnects epidermal layers so it can very precisely be dosed under pressure, it protects tissue and hyaluronic acid is more evenly distributed than with needle injections.<br>
						The result is immediately visible after application so that hyaluron is evenly distributed in lips or wrinkles filling. <br>
						Hydration is preserved and creation of new collagen is stimulated. <br>

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Theme units that we cover: 
	<br><br>
	</h2>

	<div class="row">
		<h5 style="color: white;">
			<strong>3 CLASSES </strong>- theoretical lectures: Skin, Face Anatomy, Region of Hyaluronic Pen Application Indications and Counter-Indications. 
			<br>

			<strong>7 CLASSES  </strong>- practical lessons – This part of education is done on models.
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
						<h2>
							HYALURONIC PEN ADVANTAGES:
						</h2>
					
					<h5>
						- No pin prick <br>
						- Non-invasive technique<br>
						- Non-surgical technique<br>
						- Does not leave scars <br>
						- Painless<br>
						- Short time of treatment<br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							THIS TREATMENT IS MEANT FOR ALL OF YOU WHO WANT:
						</h2>
					
					<h5>
						
						- Natural look with non-invasive technique<br>
						- Painless rejuvenation and refreshment<br>
						- Increase of lips volume<br>
						- Wrinkles filling<br>

					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					EDUCATION PRICE 
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>START</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-diamond"></i>
					</div>
					<div class="doller">
					<span>€ 490</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Education</strong>
							</li>
							<li>
								<strong>Studying Manual</strong>
							</li>
							<li>
								<strong>Certificate</strong>
							</li>
							<li>
								<strong><BR></strong>
							</li>
							<li>
								<strong><BR></strong>
							</li>
							<li>
								<strong><BR><BR></strong>
							</li>
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						BOOK  A DATE	
					</a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>SMART</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-magic-wand"></i>
					</div>
					<div class="doller">
						<span>€ 590 </span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Education</strong>
							</li>
							<li>
								<strong>Studying Manual</strong>
							</li>
							<li>
								<strong>Certificate</strong>
							</li>
							<li>
								<strong>Hyaluronic Pen Device</strong>
							</li>
							<li>
								<strong>5 Cartridges for Hyaluron<br> Pen Device</strong>
							</li>							
							<li>
								<strong>5 ml of Hyaluron </strong>
							</li>	

						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						BOOK A DATE	
					</a>
				</div>
			</div>
			
		</div>
		
		<div class="row justify-content-center">	
			<h5>
				<br>
				*
				Cena za individualnu edukaciju je za 100 eura veća u odnosu na redovnu cenu.
			</h5>
		</div>
		
		


	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop




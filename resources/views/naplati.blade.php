@extends('layout')

@section('title')
Korpa - Naplati - 
@stop

@section('scriptsTop')
	<script src="{{asset('js/klijentNaplati.js')}}"></script>
@endsection
@section('sekcije')
<section id="spabreadcrumb" class="spabreadcrumb">
		<div class="bcoverlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="Content">
						<h2>
							Naplati
						</h2>
						<div class="links">
							<ul>
								<li>
									<a href="/">Naslovna</a>
								</li>
								<li>
									<span>/</span>
								</li>
								<li>
									<a href="/korpa">Korpa</a>
								</li>
								<li>
									<span>/</span>
								</li>
								<li>
									<a class="active" href="/naplati">Naplati</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Welcome Area End -->


<!-- checkout page content area start -->
<div class="checkout-page-content-area">
	<div class="container">
		<form action="/sacuvaj_porudzbinu" class="checkout-form" method="POST" onsubmit="return potvrdiPorudzbinu()">
		<div class="row">

			<div class="col-lg-7">
				<div class="left-content-area">
					<h3 class="title">Informacije za dostavu</h3>

						{{csrf_field()}}
						<div class="row">
							<div class="col-lg-6">
								<div class="form-element">
										<label>Ime i prezime* <span class="base-color"></span></label>
										<input maxlength="254" type="text" class="input-field" placeholder="Ime i prezime" name="ime_prezime" @if(Auth::check()) value="{{Auth::user()->ime_prezime}}" @endif required>
								</div>
								<div class="form-element">
										<label>Telefon* <span class="base-color"></span></label>
										<input maxlength="19" type="text" class="input-field" placeholder="Telefon" name="telefon" @if(Auth::check()) value="{{Auth::user()->telefon}}" @endif required>
								</div>
								<div class="form-element">
										<label>Grad* <span class="base-color"></span></label>
										<input maxlength="254" type="text" class="input-field" placeholder="Grad" name="grad" @if(Auth::check()) value="{{Auth::user()->grad}}" @endif required>
								</div>
							</div>

							<div class="col-lg-6">
								<div class="form-element">
										<label>E-mail* <span class="base-color"></span></label>
										<input maxlength="254" type="email" class="input-field" placeholder="E-mail" name="email" @if(Auth::check()) value="{{Auth::user()->email}}" @endif required>
								</div>
								<div class="form-element">
										<label>Adresa* <span class="base-color"></span></label>
										<input maxlength="254" type="text" class="input-field" placeholder="Adresa" name="adresa" @if(Auth::check()) value="{{Auth::user()->adresa}}" @endif required>
								</div>
								<div class="form-element">
										<label>Poštanski broj*  <span class="base-color"></span></label>
										<input type="number" min="10000" max="99999" class="input-field" placeholder="Poštanski broj"  name="zip" @if(Auth::check()) value="{{Auth::user()->zip}}" @endif required>
								</div>
							</div>
							
									
							<div class="col-lg-12">
								<div class="shipping-details">
									<div class="form-element textarea">
										<label>Napomena</label>
										<textarea maxlength="999" placeholder="Napomena, broj stana, sprat..." class="input-field textarea" cols="30" rows="10" name="napomena"></textarea>
									</div>
								</div><!-- //. shipping details -->
							</div>
						</div>

				</div>
			</div>
			<div class="col-lg-5">
				<div class="right-content-area">
					<h3 class="title">Vaša porudžbina</h3>
					<ul class="order-list">
						<li>
							<div class="single-order-list title-bold">
									Cena <span class="right normal">{{number_format($total, 0, ',', '.')}} rsd</span>
							</div>
						</li>
						<li class="shipping">
							<div class="single-order-list title-bold">
								Dostava <span class="right">{{number_format(290, 0, ',', '.')}} rsd</span>
								<!-- <span class="normal-text">Besplatna dostava za porudžbine preko 2,999 rsd</span> -->
							</div>
						</li>
						
						<li>
							<div class="single-order-list title-bold">
									<STRONG>UKUPNO </STRONG>
									<span class="right normal"><STRONG>{{number_format($total + 290, 0, ',', '.')}} rsd</STRONG></span>
							</div>
						</li>
					</ul>
					<div class="checkbox-element account">
						<div class="checkbox-wrapper">
							<label class="checkboxContainer">
								<span class="labelText">
									Slažem se sa
							</span>
								<input type="checkbox" name="uslovi" id="usloviKoriscenjaCB">
								<span class="checkmark"></span>

							</label>
							<!-- TODO -->
							<a href="/uslovi-kupovine" class="base-color">uslovima kupovine*</a>
						</div>
					</div>
					<div class="btn-wrapper">
							<button type="submit" class="submit-btn"> Potvrdi porudžbinu </button>
					</div>
					<p id="uslovi-error" style="color:red; display:none;">Morate prihvatiti uslove korišćenja.</p>
				</div>
			</div>

		</div>
		</form>
	</div>
</div>
<!-- checkout page content area end -->
@stop
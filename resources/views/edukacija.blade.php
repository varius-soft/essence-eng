@extends('layout')

@section('title')
Edukacija - 
@stop

@section('sekcije')
<section id="spabreadcrumb" class="spabreadcrumb extrapadding">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						EDUCATION
					</h2>
					<div class="links">
					<ul>
						<li>
							<a href="/">Home</a>
						</li>
						<li>
							<span>/</span>
						</li>
						<li>
							<a class="active" href="/edukacija">Education</a>
						</li>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->

<!-- Main Blog Sectin Srart -->
<section id="mainBlog" class="mainBlog">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/bb-glow">
						<img class="img-fluid" src="img/edukacija/bb-glow.png" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/bb-glow" title="BB Glow tretman edukacija">BB Glow</a></h3>
						<h3 class="post-date">PRICE: RSD 22,500</h3>
						<div>
							<p>Do you wish to provide your clients the treatment that even Hollywood stars are crazy about, but you do not know which educator should you give your time and money to, in order to be sure to successfully master this technique and be one of the first to become certified highly professional BB Glow therapist?</p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/bb-glow">View details<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/hijaluron-pen">
						<img class="img-fluid" src="img/edukacija/hijaluron-pen.jpg" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/hijaluron-pen " title="BB Glow tretman edukacija">Hyaluronic Pen</a></h3>
						<h3 class="post-date">PRICE: RSD 58,000</h3>
						<div>
							<p>
This method is gentle and almost painless and it reconnects cuticular layers so that it can be precisely and under pressure dosed, protects tissue and hyaluronic acid is more evenly distributed than with needle injections. The result is visible immediately after application so that hyaluron is evenly distributed in lips or wrinkles filling. </p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/hijaluron-pen">View details<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/maderoterapija">
						<img class="img-fluid" src="img/edukacija/maderoterapija.png" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/maderoterapija" title="BB Glow tretman edukacija">Madero Therapy</a></h3>
						<h3 class="post-date">PRICE: RSD 25,000</h3>
						<div>
							<p>You have heard that anti-cellulite massage with a rolling pin is the most effective and most natural way for cellulite elimination, and also the most wanted service in beauty parlors? You really want to do this very profitable job but you have no one to learn it from? Please contact us as soon as possible since today starts application for training for Madero Therapy Therapist in our Educational Center.</p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/maderoterapija">View details<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/maderoterapija-lica">
						<img class="img-fluid" src="img/edukacija/maderoterapija-lica.png" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/maderoterapija-lica" title="BB Glow tretman edukacija">Face Madero Therapy</a></h3>
						<h3 class="post-date">PRICE: RSD 12,000</h3>
						<div>
							<p>Apart from Body Madero Therapy which has absolutely become the most popular technique for cellulite removal, Face Madero Therapy is also becoming more and more wanted treatment in beauty parlors. Those who have tried Facial Madero Therapy are convinced that it brings improvement of face skin tonus and give rejuvenating effect since it stimulates the production of elastin and collagen.</p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/maderoterapija-lica">View details<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/anticelulit-masaza">
						<img class="img-fluid" src="img/edukacija/anticelulit-masaza.jpg" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/anticelulit-masaza" title="BB Glow tretman edukacija">Anti-cellulite Massage</a></h3>
						<h3 class="post-date">PRICE: RSD 25,000</h3>
						<div>
							<p>Among women there is a popular belief that hand anti-cellulite massage is the most efficient way for cellulite removal. The goal of our education is to teach you how to correctly apply anti-cellulite massage in order to get rid of the so-called orange peel on the client’s body.</p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/anticelulit-masaza">View details<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/relaks-masaza">
						<img class="img-fluid" src="img/edukacija/relaks-masaza.png" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						
						<h3 class="title"><a href="/edukacija/relaks-masaza" title="BB Glow tretman edukacija">Relax Massage</a></h3>
						<h3 class="post-date">PRICE: RSD 27,000</h3>
						<div>
							<p>If you have so far shown tendency towards massage and you really want to do it, this is your unique opportunity to master this relax massage. Due to everyday ubiquitous stress, greater number of people feels exhausted, tense and has great need for relaxation. We will teach you how to transfer on your clients your positive energy and in that way help them improve their psycho-physical condition.</p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/relaks-masaza">View details<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/svedska-masaza">
						<img class="img-fluid" src="img/edukacija/svedska-masaza.jpg" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/svedska-masaza" title="BB Glow tretman edukacija">Swedish Massage</a></h3>
						<h3 class="post-date">PRICE: RSD 18,000</h3>
						<div>
							<p>Swedish massage is applied in combination with gymnastics – passive and active exercises. The goal of those exercises is removal of painful condition in wrists and getting back mobility. With massage you remove tension in muscles as well, stress and problems with peripheral blood circulation and it provides better oxygen flow into the body.  </p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/svedska-masaza">View details<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/refleksoloska-masaza">
						<img class="img-fluid" src="img/edukacija/refleksoloska-masaza.jpg" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/refleksoloska-masaza" title="BB Glow tretman edukacija">Reflexive Massage</a></h3>
						<h3 class="post-date">PRICE: RSD 18,000</h3>
						<div>
							<p>Reflexive Massage is applied on neuro-reflexive zones on hands and feet, each of which is connected with one of organs. Presence of pain in some of those spots implies a specific problem with specific organ.  </p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/refleksoloska-masaza">View details<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/masaza-svecom">
						<img class="img-fluid" src="img/edukacija/masaza-svecom.jpg" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/masaza-svecom" title="BB Glow tretman edukacija">Candle Massage</a></h3>
						<h3 class="post-date">PRICE: RSD 18,000</h3>
						<div>
							<p> 
Candles made out of soya wax, which do not produce irritation and burns on skin are applied in this massage. With this massage we achieve skin tightening, improvement of circulation, removal of “dead” cells and the skin becomes smooth. The candle is lighted and it partially melts, wax is poured all over body and the massage is applied. </p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/masaza-svecom">View details<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/depilacija">
						<img class="img-fluid" src="img/edukacija/depilacija.jpg" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/depilacija" title="BB Glow tretman edukacija">Depilation</a></h3>
						<h3 class="post-date">PRICE: RSD 17,000</h3>
						<div>
							<p>Cold wax depilation is one of the most wanted services in beauty parlors. It is the procedure for thorough removal of hairs in the region of face or body, as required by client. Wax is applied in a thin and even layer on the skin surface, on which a strip is pressed and finally using quick movement, removed together with hairs. </p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/depilacija">View details<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Main Blog Sectin End -->
@stop
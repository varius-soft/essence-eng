@extends('admin.adminLayout')

@section('title')
    Proizvodi
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <span class="breadcrumb-item active">Proizvodi</span>
@stop

@section('heder-h1')
Proizvodi
@stop


@section('heder-h2')
Trenutno <a class="text-primary-light link-effect">{{$brojNaAkciji}} proizvoda na akciji</a>.
@stop

@section('scriptsTop')
    <script src="{{asset('/js/adminProizvodi.js')}}"></script>
@endsection
@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('/js/tabelaProizvodi.js')}}"></script>
@endsection

@section('main')
    <div class="row gutters-tiny">
        <!-- All Products -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:void(0)">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-circle-o fa-2x text-info-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{count($proizvodi) + count($obrisaniProizvodi)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno proizvoda</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END All Products -->

        <!-- Top Sellers -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziDostupne()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-star fa-2x text-warning-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{count($proizvodi)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Dostupno</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Top Sellers -->

        <!-- Out of Stock -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziNedostupne()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-warning fa-2x text-danger-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-danger" data-toggle="countTo" data-to="{{count($obrisaniProizvodi)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Obrisano</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Out of Stock -->

        <!-- Add Product -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="/admin/proizvod/-1">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-archive fa-2x text-success-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Dodaj novi proizvod</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Add Product -->
    </div>
    <!-- END Overview -->

    <!-- Dynamic Table Full Pagination -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title" id="proizvodi-avail-title">Dostupni proizvodi</h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
            <table id="dostupni-tabela" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center">Šifra</th>
                    <th>Naziv</th>
                    <th class="d-none d-sm-table-cell">Cena</th>
                    <th class="d-none d-sm-table-cell">Brend</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Status</th>
                    <th class="text-center" style="width: 15%;">Akcija</th>
                </tr>
                </thead>
                <tbody>
                @foreach($proizvodi as $proizvod)
                    <tr>
                        <td class="text-center">{{$proizvod->sifra}}</td>
                        <td class="font-w600">{{$proizvod->naziv}}</td>
                        <td class="d-none d-sm-table-cell">@if($proizvod->na_popustu) {{number_format($proizvod->cena_popust, 2, ',', '.')}} @else {{number_format($proizvod->cena, 2, ',', '.')}} @endif</td>
                        <td class="d-none d-sm-table-cell">{{$proizvod->brend}}</td>
                        <td class="d-none d-sm-table-cell">
                            @foreach($proizvod->opisi as $opis)
                                <span class="badge" style="background-color:{{$opis->boja_pozadine}}; color:{{$opis->boja_slova}}">{{$opis->opis}}</span>
                            @endforeach
                        </td>
                        <td class="text-center">
                            <a href="/admin/proizvod/{{$proizvod->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Izmeni proizvod">
                                <i class="fa fa-edit"></i>
                            </a>
                            <form action="/admin/obrisiProizvod/{{$proizvod->id}}" method="POST" style="display: inline;">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Obriši proizovd">
                                    <i class="fa fa-times"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <table id="obrisani-tabela" style="display: none" class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
            <tr>
                <th class="text-center">Šifra</th>
                <th>Naziv</th>
                <th class="d-none d-sm-table-cell">Cena</th>
                <th class="d-none d-sm-table-cell">Brend</th>
                <th class="d-none d-sm-table-cell" style="width: 15%;">Status</th>
                <th class="text-center" style="width: 15%;">Akcija</th>
            </tr>
            </thead>
            <tbody>
            @foreach($obrisaniProizvodi as $proizvod)
                <tr>
                    <td class="text-center">{{$proizvod->sifra}}</td>
                    <td class="font-w600">{{$proizvod->naziv}}</td>
                    <td class="d-none d-sm-table-cell">@if($proizvod->na_popustu) {{$proizvod->cena_popust}} @else {{$proizvod->cena}} @endif</td>
                    <td class="d-none d-sm-table-cell">{{$proizvod->brend}}</td>
                    <td class="d-none d-sm-table-cell">
                        @foreach($proizvod->opisi as $opis)
                            <span class="badge" style="background-color:{{$opis->boja_pozadine}}; color:{{$opis->boja_slova}}">{{$opis->opis}}</span>
                        @endforeach
                    </td>
                    <td class="text-center">
                        <form action="/admin/proizvod/{{$proizvod->id}}" method="GET" style="display: inline;">
                            <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Izmeni proizvod">
                                <i class="fa fa-edit"></i>
                            </button>
                        </form>
                        <form action="/admin/restaurirajProizvod/{{$proizvod->id}}" method="POST" style="display: inline;">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Restauriraj proizovd">
                                <i class="fa fa-undo"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full Pagination -->
@stop
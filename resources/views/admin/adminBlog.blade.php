@extends('admin.adminLayout')

@section('title')
Blog
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/">Admin</a>
<span class="breadcrumb-item active">Blog</span>
@stop

@section('heder-h1')
Blog
@stop


@section('heder-h2')
Trenutno <a class="text-primary-light link-effect">{{count($blog)}} aktivnih članaka</a>.
@stop

@section('scriptsTop')
    <script src="{{asset('/js/adminClanci.js')}}"></script>
@endsection

@section('scriptsBottom')
    <script src="{{asset('/js/tabelaClanci.js')}}"></script>
@endsection

@section('main')
<div class="row gutters-tiny">
    <!-- All Products -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-file fa-2x text-info-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{count($blog) + count($obrisani)}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno članaka</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END All Products -->
    <!-- Top Sellers -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziDostupne()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-star fa-2x text-warning-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{count($blog)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Dostupnih članaka</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Top Sellers -->
    <!-- Out of Stock -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziNedostupne()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-warning fa-2x text-danger-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-danger" data-toggle="countTo" data-to="{{count($obrisani)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Obrisanih članaka</div>
                    </div>
                </div>
            </a>
        </div>
    <!-- Add Product -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow" href="/admin/clanak/-1">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-archive fa-2x text-success-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-success">
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Dodaj novi članak</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Add Product -->


    
</div>
<!-- END Overview -->

<!-- Dynamic Table Full Pagination -->
<div class="block">
    <div class="block-header block-header-default">
        <h3 id="clanci-title" class="block-title">Dostupni članci</h3>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
        <table id="tabela-clanci-aktivni" class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    
                    <th style="width: 46%;">Naslov</th>
                    <th class="text-center" style="width: 46%;">Datum</th>
                    
                    <th class="text-center" style="width: 100%;">Akcija</th>
                </tr>
            </thead>
            <tbody>
                @foreach($blog as $clanak)
                <tr>
                    <td class="">{{$clanak->naslov}}</td>
                    <td class="text-center font-w600">{{$clanak->created_at}}</td>
                    
                    <td class="text-center">
                        <a href="/admin/clanak/{{$clanak->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Izmeni članak">
                            <i class="fa fa-edit"></i>
                        </a>
                        <form method="POST" action="/admin/obrisiClanak/{{$clanak->id}}" style="display:inline">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Obriši članak">
                                <i class="fa fa-times"></i>
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach
               
            </tbody>
        </table>

        <table id="tabela-clanci-obrisani" class="table table-bordered table-striped table-vcenter js-dataTable-full" style="display:none;">
            <thead>
            <tr>

                <th style="width:46%;">Naslov</th>
                <th class="text-center" style="width:46%;">Datum</th>

                <th class="text-center" style="width: 100%;">Akcija</th>
            </tr>
            </thead>
            <tbody>
            @foreach($obrisani as $clanak)
                <tr>
                    <td class="">{{$clanak->naslov}}</td>
                    <td class="text-center font-w600">{{$clanak->created_at}}</td>

                    <td class="text-center">
                        <a href="/admin/clanak/{{$clanak->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Izmeni članak">
                            <i class="fa fa-edit"></i>
                        </a>
                        <form method="POST" action="/admin/restaurirajClanak/{{$clanak->id}}" style="display:inline">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Restauriraj članak">
                                <i class="fa fa-undo"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->
@stop
@extends('admin.adminLayout')

@section('title')
    @if($izmena)
        Dobavljač - {{$dobavljac->naziv}}
    @else
        Novi dobavljač
    @endif
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<a class="breadcrumb-item" href="/admin/dobavljaci">Dobavljači</a>
<span class="breadcrumb-item active">@if($izmena){{$dobavljac->naziv}} @else Novi dobavljač @endif</span>
@stop

@section('heder-h1')
@if($izmena){{$dobavljac->naziv}} @else Novi dobavljač @endif
@stop


@section('main')
<div class="row gutters-tiny">
@if($izmena)

    <!-- In Orders -->
    <div class="col-md-3 col-xl-3">
        <a class="block block-rounded block-link-shadow" >
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-shopping-basket fa-2x text-info"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{$dobavljac->broj_prodatih}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Prodatih proizvoda</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END In Orders -->

    <!-- Stock -->
    <div class="col-md-3 col-xl-3">
        <a class="block block-rounded block-link-shadow" >
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="si si-social-dropbox fa-2x text-warning"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{$dobavljac->broj_na_stanju}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Proizvoda na stanju</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Stock -->
@endif
    <!-- Stock -->
    <div class="col-md-3 col-xl-3">

        <a class="block block-rounded block-link-shadow" href="javascript:$('#forma-dobavljac-submit-button').click()">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="si si-settings fa-2x text-success"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-success">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Sačuvaj</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Stock -->

    @if($izmena)
        @if(!$dobavljac->sakriven)
        <!-- Delete Product -->
        <div class="col-md-3 col-xl-3">
            <form id="forma-obrisi-dobavljaca" method="POST" action="/admin/obrisiDobavljaca/{{$dobavljac->id}}">
            {{csrf_field()}}
            <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-obrisi-dobavljaca').submit();">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-trash fa-2x text-danger"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-danger">
                            <i class="fa fa-times"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Obriši dobavljača</div>
                    </div>
                </div>
            </a>
            </form>
        </div>

        @else
        <div class="col-md-3 col-xl-3">
            <form id="forma-restauriraj-dobavljaca" method="POST" action="/admin/restaurirajDobavljaca/{{$dobavljac->id}}">
                {{csrf_field()}}
            <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj-dobavljaca').submit();">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-lightbulb-o fa-2x text-warning"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning">
                            <i class="fa fa-undo"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Restauriraj dobavljača</div>
                    </div>
                </div>
            </a>
            </form>
        </div>
        @endif
    @endif
    <!-- END Delete Product -->
</div>
<!-- END Overview -->
<form id="forma-dobavljac" method="POST" @if($izmena) action="/admin/sacuvajDobavljaca/{{$dobavljac->id}}" @else action="/admin/sacuvajDobavljaca/-1" @endif>
{{csrf_field()}}
<!-- Update Product -->
<h2 class="content-heading">Informacije o dobavljaču</h2>
<div class="row gutters-tiny">
    <!-- Basic Info -->
    <div class="col-md-7">
            <div class="block block-rounded block-themed">
                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">Informacije</h3>
                </div>
                <div class="block-content block-content-full">
                	<div class="form-group row">
                        <label class="col-12" >Naziv</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="si si-info"></i>
                                </span>
                            </div>
                            <input maxlength="254" type="text" class="form-control" name="naziv" @if($izmena) value="{{$dobavljac->naziv}}" @endif>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12">Šifra dobavljača</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-barcode"></i>
                                </span>
                            </div>
                            <input maxlength="254" type="text" class="form-control" name="sifra"  @if($izmena) value="{{$dobavljac->sifra}}" @endif>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12" for="ecom-product-name1">Adresa</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-home"></i>
                                </span>
                            </div>
                            <input maxlength="254" type="text" class="form-control" name="adresa" @if($izmena) value="{{$dobavljac->adresa}}" @endif>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12">Grad</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="si si-globe-alt"></i>
                                </span>
                            </div>
                            <input maxlength="254" type="text" class="form-control" name="grad" @if($izmena) value="{{$dobavljac->grad}}" @endif>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12">Poštanski broj</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="si si-map"></i>
                                </span>
                            </div>
                            <input type="number" maxlength="5" type="text" class="form-control" name="zip" @if($izmena) value="{{$dobavljac->zip}}" @endif>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-12">Opis</label>
                        <div class="col-12">
                            <!-- CKEditor (js-ckeditor id is initialized in Codebase() -> uiHelperCkeditor()) -->
                            <!-- For more info and examples you can check out http://ckeditor.com -->
                            <textarea maxlength="9999" class="form-control" id="js-ckeditor" name="opis" rows="8">@if($izmena) {{$dobavljac->opis}} @endif</textarea>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
    </div>

    <!-- END Basic Info -->

    <!-- More Options -->
    <div class="col-md-5">
        <!-- Status -->
            <div class="block block-rounded block-themed">
                <div class="block-header bg-gd-primary">
                    <h3 class="block-title">Kontakt</h3>
                    
                </div>

                <div class="block-content block-content-full">
                    <div class="form-group row">
                        <label class="col-12">Telefon 1</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-phone"></i>
                                </span>
                            </div>
                            <input maxlength="19" type="text" class="form-control" name="broj_telefona" @if($izmena) value="{{$dobavljac->broj_telefona}}" @endif>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-12">Telefon 2</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-phone"></i>
                                </span>
                            </div>
                            <input maxlength="19"  type="text" class="form-control" name="broj_telefona2" @if($izmena) value="{{$dobavljac->broj_telefona2}}" @endif>
                        </div>
                    </div>
					
                    <div class="form-group row">
                        <label class="col-12">E-Mail 1</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-envelope"></i>
                                </span>
                            </div>
                            <input type="email" maxlength="254" class="form-control"  name="email" @if($izmena) value="{{$dobavljac->email}}" @endif>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-12">E-Mail 2</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-envelope"></i>
                                </span>
                            </div>
                            <input type="email" maxlength="254" class="form-control" name="email2" @if($izmena) value="{{$dobavljac->email2}}" @endif>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12">Fax</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-fax"></i>
                                </span>
                            </div>
                            <input maxlength="254" type="text" class="form-control" name="fax"  @if($izmena) value="{{$dobavljac->fax}}" @endif>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12" >PIB</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <strong>PIB</strong>
                                </span>
                            </div>
                            <input maxlength="19" type="text" class="form-control"  name="PIB"  @if($izmena) value="{{$dobavljac->PIB}}" @endif>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-12 " for="ecom-product-pp">Matični broj</label>
                        <div class="col-12 input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <strong>MB</strong>
                                </span>
                            </div>
                            <input maxlength="19" type="text" class="form-control" name="maticni_broj"  @if($izmena) value="{{$dobavljac->maticni_broj}}" @endif>
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>

     
    </div>
    <!-- END More Options -->
</div>
<!-- END Update Product -->
<input type="submit" id="forma-dobavljac-submit-button" style="display:none"/>
</form>
@stop
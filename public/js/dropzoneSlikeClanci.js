var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
Dropzone.autoDiscover = false;

function initDropzone( maxFiles){
    $('#glavna-dropzone-div').dropzone({
        url: "/admin/clanak/uploadSlike",
        autoProcessQueue: true,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: maxFiles,
        acceptedFiles: "image/*",
        headers: {
            'x-csrf-token': CSRF_TOKEN,
        },
        init: function () {

            var wrapperThis = this;


            this.on("addedfile", function (file) {

                // Create the remove button
                var removeButton = Dropzone.createElement("<button class='btn btn-lg dark offset-2' style='margin-top:2px;'>Obriši</button>");

                // Listen to the click event
                removeButton.addEventListener("click", function (e) {
                    // Make sure the button click doesn't submit the form:
                    //e.preventDefault();
                    //e.stopPropagation();

                    // Remove the file preview.

                    wrapperThis.removeFile(file);
                    // If you want to the delete the file on the server as well,
                    // you can do the AJAX request here.
                    $.ajax({
                        url: "/admin/clanak/obrisiUploadSlike",
                        type: "POST",
                        data: {
                            "filename": file.name,
                        }
                    });
                });

                // Add the button to the file preview element.
                file.previewElement.appendChild(removeButton);

                // izbaci postojecu glavnu sliku ukoliko se radi izmena
                if(document.getElementById('slike-glavna.jpg') != null){
                    obrisiSliku('glavna.jpg');
                }
            });
        }
    });
}

initDropzone( 1);